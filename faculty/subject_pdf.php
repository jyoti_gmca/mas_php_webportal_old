<?php
session_start();
$a=$_SESSION['sub'];
$b=$_SESSION['sem'];
$c=$_SESSION['div'];
$d=$_SESSION['day'];
$e=$_SESSION['time'];
$f=$_SESSION['times'];


require('FPDF/fpdf.php');

$servername="localhost";
$username = "root";
$password = "";
$dbname="cteict";
$conn =mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed:" . $conn->connect_error);
} 
class PDF extends FPDF
{
	function Header()
	{
		$this->SetFont('Arial','B',15);
		$this->Cell(12);
			
		
		$this->Ln(5);
		$this->SetFont('Arial','B',11);
		/*$this->SetFillColor(160,180,255);
		$this->SetDrawColor(50,50,100);
		//$this->Cell(150,5,'OrderID',1,0,'C',true);
		
		$this->Cell(40,5,'',1,0,'C',true);
		$this->Cell(40,5,'',1,0,'C',true);
		$this->Cell(40,5,'',1,0,'C',true);
		$this->Cell(30,5,'',1,1,'C',true);
	*/
	}
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','',8);
		
		$this->Cell(190,0,'','T',1,'',true);
		$this->Cell(0,10,'Page '.$this->PageNo()."/ {pages}",0,0,'C');
		
	}
	
}
$pdf=new PDF();
$pdf->AddPage();
$pdf->Image("FPDF/145.jpg",0,0,210);
$pdf->SetAutoPageBreak(true,15);

$pdf->AliasNbPages('{pages}');
$pdf->SetFont('Arial','B',12);
$pdf->SetFillColor(180,180,255);
	$pdf->SetDrawColor(50,50,100);
		$pdf->Ln(150);
		$pdf->SetTextColor(255,167,69);
		
		$pdf->ln(10);
		$pdf->Cell(80,11,'Absent',1,1,'C');
		$pdf->Cell(40,11,'RollNo',1,0,'C');
		$pdf->Cell(40,11,'Name',1,1,'C');
	
	
		$pdf->SetFont('Arial','',11);
		$pdf->SetTextColor(85,120,255);
		
		$query = "SELECT STUDENT_ID,STUDENT_NAME from student_details where SEM_ID='$b' AND DIVISION='$c' AND STUDENT_ID NOT IN (SELECT STUDENT_ID FROM attendance where T_ID IN (SELECT T_ID FROM time_table WHERE T_DAY='$d' AND T_START_TIME='$e' AND T_END_TIME='$f' AND SUBJECT_ID='$a'))";
		$sql=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($sql))
		{
		$pdf->Cell(40,9,$row[0],1,0,'C');
		$pdf->Cell(40,9,$row[1],1,1,'C');
		}
		
		$pdf->SetDrawColor(50,50,100);
		$pdf->Ln(05);
		$pdf->SetTextColor(255,167,69);
		$pdf->SetFont('Arial','B',11);
		$pdf->ln(10);
	
		$pdf->Cell(80,11,'Present',1,1,'C');
		$pdf->Cell(40,11,'RollNo',1,0,'C');
		$pdf->Cell(40,11,'Name',1,1,'C');
	
	
		
		$pdf->SetTextColor(85,120,255);
		
		$query = "SELECT STUDENT_ID,STUDENT_NAME from student_details where SEM_ID='$b' AND DIVISION='$c' AND STUDENT_ID  IN (SELECT STUDENT_ID FROM attendance where T_ID IN (SELECT T_ID FROM time_table WHERE T_DAY='$d' AND T_START_TIME='$e' AND T_END_TIME='$f' AND SUBJECT_ID='$a'))";
		$sql=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($sql))
		{
		$pdf->Cell(40,9,$row[0],1,1,'C');
		$pdf->Cell(40,9,$row[1],1,1,'C');
		}
	$pdf->Output();
?>