-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2018 at 08:10 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cteict`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `A_id` int(11) NOT NULL,
  `Reg_id` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `Date` date NOT NULL DEFAULT '2018-10-08',
  `EndTime` timestamp NOT NULL DEFAULT '2018-10-07 18:30:00',
  `StartTime` timestamp NULL DEFAULT '2018-10-08 06:05:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`A_id`, `Reg_id`, `Status`, `Date`, `EndTime`, `StartTime`) VALUES
(96, 1, 1, '2018-10-01', '2018-10-01 12:45:07', '2018-10-01 04:42:42'),
(97, 2, 1, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(98, 3, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(99, 4, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(100, 117, 1, '2018-10-01', '2018-10-01 12:11:16', '2018-10-01 05:17:32'),
(101, 118, 1, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(102, 119, 1, '2018-10-01', '2018-10-01 12:00:10', '0000-00-00 00:00:00'),
(103, 120, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(104, 121, 1, '2018-10-01', '2018-10-01 11:50:17', '2018-10-01 04:45:28'),
(105, 122, 1, '2018-10-01', '2018-10-01 11:47:38', '2018-10-01 04:54:41'),
(106, 123, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(107, 12, 1, '2018-10-01', '2018-10-01 12:07:20', '2018-10-01 05:09:51'),
(108, 3918, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(109, 2534, 1, '2018-10-01', '2018-10-01 12:15:30', '2018-10-01 05:01:51'),
(110, 2528, 1, '2018-10-01', '2018-10-01 11:32:10', '2018-10-01 05:25:53'),
(111, 2535, 1, '2018-10-01', '2018-10-01 11:36:19', '2018-10-01 05:08:03'),
(112, 2471, 0, '2018-10-01', '2018-10-01 11:42:47', '2018-10-01 05:15:21'),
(113, 1, 1, '2018-10-02', '2018-10-02 12:45:07', '2018-10-02 04:42:42'),
(114, 2, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(115, 3, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(116, 4, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(117, 117, 1, '2018-10-02', '2018-10-02 12:11:16', '2018-10-02 05:17:32'),
(118, 118, 0, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(119, 119, 1, '2018-10-02', '2018-10-02 12:00:10', '0000-00-00 00:00:00'),
(120, 120, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(121, 121, 1, '2018-10-02', '2018-10-02 11:50:17', '2018-10-02 04:45:28'),
(122, 122, 0, '2018-10-02', '2018-10-02 11:47:38', '2018-10-02 04:54:41'),
(123, 123, 0, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(124, 12, 0, '2018-10-02', '2018-10-02 12:07:20', '2018-10-02 05:09:51'),
(125, 3918, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(126, 2534, 0, '2018-10-02', '2018-10-02 12:15:30', '2018-10-02 05:01:51'),
(127, 2528, 1, '2018-10-02', '2018-10-02 11:32:10', '2018-10-02 05:25:53'),
(128, 2535, 0, '2018-10-02', '2018-10-02 11:36:19', '2018-10-02 05:08:03'),
(129, 2471, 1, '2018-10-02', '2018-10-02 11:42:47', '2018-10-02 05:15:21'),
(130, 1, 0, '2018-10-03', '2018-10-03 12:45:07', '2018-10-03 04:42:42'),
(131, 2, 1, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(132, 3, 0, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(133, 4, 0, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(134, 117, 1, '2018-10-03', '2018-10-03 12:11:16', '2018-10-03 05:17:32'),
(135, 118, 0, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(136, 119, 1, '2018-10-03', '2018-10-03 12:00:10', '0000-00-00 00:00:00'),
(137, 120, 1, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(138, 121, 1, '2018-10-03', '2018-10-03 11:50:17', '2018-10-03 04:45:28'),
(139, 122, 0, '2018-10-03', '2018-10-03 11:47:38', '2018-10-03 04:54:41'),
(140, 123, 0, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(141, 12, 0, '2018-10-03', '2018-10-03 12:07:20', '2018-10-03 05:09:51'),
(142, 3918, 1, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(143, 2534, 0, '2018-10-03', '2018-10-03 12:15:30', '2018-10-03 05:01:51'),
(144, 2528, 1, '2018-10-03', '2018-10-03 11:32:10', '2018-10-03 05:25:53'),
(145, 2535, 0, '2018-10-03', '2018-10-03 11:36:19', '2018-10-03 05:08:03'),
(146, 2471, 1, '2018-10-03', '2018-10-03 11:42:47', '2018-10-03 05:15:21'),
(147, 1, 1, '2018-10-04', '2018-10-04 12:45:07', '2018-10-04 04:42:42'),
(148, 2, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(149, 3, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(150, 4, 0, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(151, 117, 1, '2018-10-04', '2018-10-04 12:11:16', '2018-10-04 05:17:32'),
(152, 118, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(153, 119, 1, '2018-10-04', '2018-10-04 12:00:10', '0000-00-00 00:00:00'),
(154, 120, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(155, 121, 1, '2018-10-04', '2018-10-04 11:50:17', '2018-10-04 04:45:28'),
(156, 122, 0, '2018-10-04', '2018-10-04 11:47:38', '2018-10-04 04:54:41'),
(157, 123, 0, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(158, 12, 1, '2018-10-04', '2018-10-04 12:07:20', '2018-10-04 05:09:51'),
(159, 3918, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(160, 2534, 0, '2018-10-04', '2018-10-04 12:15:30', '2018-10-04 05:01:51'),
(161, 2528, 1, '2018-10-04', '2018-10-04 11:32:10', '2018-10-04 05:25:53'),
(162, 2535, 1, '2018-10-04', '2018-10-04 11:36:19', '2018-10-04 05:08:03'),
(163, 2471, 1, '2018-10-04', '2018-10-04 11:42:47', '2018-10-04 05:15:21'),
(164, 1, 1, '2018-10-05', '2018-10-05 12:45:07', '2018-10-05 04:42:42'),
(165, 2, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(166, 3, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(167, 4, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(168, 117, 1, '2018-10-05', '2018-10-05 12:11:16', '2018-10-05 05:17:32'),
(169, 118, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(170, 119, 1, '2018-10-05', '2018-10-05 12:00:10', '0000-00-00 00:00:00'),
(171, 120, 1, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(172, 121, 1, '2018-10-05', '2018-10-05 11:50:17', '2018-10-05 04:45:28'),
(173, 122, 0, '2018-10-05', '2018-10-05 11:47:38', '2018-10-05 04:54:41'),
(174, 123, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(175, 12, 1, '2018-10-05', '2018-10-05 12:07:20', '2018-10-05 05:09:51'),
(176, 3918, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(177, 2534, 0, '2018-10-05', '2018-10-05 12:15:30', '2018-10-05 05:01:51'),
(178, 2528, 1, '2018-10-05', '2018-10-05 11:32:10', '2018-10-05 05:25:53'),
(179, 2535, 0, '2018-10-05', '2018-10-05 11:36:19', '2018-10-05 05:08:03'),
(180, 2471, 0, '2018-10-05', '2018-10-05 11:42:47', '2018-10-05 05:15:21'),
(181, 1, 1, '2018-10-06', '2018-10-06 12:45:07', '2018-10-06 04:42:42'),
(182, 2, 1, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(183, 3, 1, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(184, 4, 1, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(185, 117, 1, '2018-10-06', '2018-10-06 12:11:16', '2018-10-06 05:17:32'),
(186, 118, 0, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(187, 119, 1, '2018-10-06', '2018-10-06 12:00:10', '0000-00-00 00:00:00'),
(188, 120, 1, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(189, 121, 1, '2018-10-06', '2018-10-06 11:50:17', '2018-10-06 04:45:28'),
(190, 122, 0, '2018-10-06', '2018-10-06 11:47:38', '2018-10-06 04:54:41'),
(191, 123, 0, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(192, 12, 1, '2018-10-06', '2018-10-06 12:07:20', '2018-10-06 05:09:51'),
(193, 3918, 0, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(194, 2534, 0, '2018-10-06', '2018-10-06 12:15:30', '2018-10-06 05:01:51'),
(195, 2528, 1, '2018-10-06', '2018-10-06 11:32:10', '2018-10-06 05:25:53'),
(196, 2535, 0, '2018-10-06', '2018-10-06 11:36:19', '2018-10-06 05:08:03'),
(197, 2471, 0, '2018-10-06', '2018-10-06 11:42:47', '2018-10-06 05:15:21'),
(198, 1, 0, '2018-10-08', '2018-10-08 12:45:07', '2018-10-08 04:42:42'),
(199, 2, 0, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(200, 3, 1, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(201, 4, 1, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(202, 117, 1, '2018-10-08', '2018-10-08 12:11:16', '2018-10-08 05:17:32'),
(203, 118, 0, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(204, 119, 1, '2018-10-08', '2018-10-08 12:00:10', '0000-00-00 00:00:00'),
(205, 120, 1, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(206, 121, 1, '2018-10-08', '2018-10-08 11:50:17', '2018-10-08 04:45:28'),
(207, 122, 0, '2018-10-08', '2018-10-08 11:47:38', '2018-10-08 04:54:41'),
(208, 123, 0, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(209, 12, 1, '2018-10-08', '2018-10-08 12:07:20', '2018-10-08 05:09:51'),
(210, 3918, 0, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21'),
(211, 2534, 0, '2018-10-08', '2018-10-08 12:15:30', '2018-10-08 05:01:51'),
(212, 2528, 1, '2018-10-08', '2018-10-08 11:32:10', '2018-10-08 05:25:53'),
(213, 2535, 0, '2018-10-08', '2018-10-08 11:36:19', '2018-10-08 05:08:03'),
(214, 2471, 0, '2018-10-08', '2018-10-08 11:42:47', '2018-10-08 05:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_auth`
--

CREATE TABLE `attendance_auth` (
  `AA_ID` int(11) NOT NULL,
  `LOGINID` int(11) NOT NULL,
  `BSSID` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SSID` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAC` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IMEID` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ANDROIDID` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IP` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STATUS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance_auth`
--

INSERT INTO `attendance_auth` (`AA_ID`, `LOGINID`, `BSSID`, `SSID`, `MAC`, `IMEID`, `ANDROIDID`, `IP`, `STATUS`) VALUES
(1, 1, 'AC:24:47:DB:24', 'AC:24:47:DB:24', 'AC:24:47:DB:24', 'AC:24:47:DB:24', 'AC:24:47:DB:24', 'AC:24:47:DB:24', 1),
(4, 5, '02:00:00:00:00:00', '<unknown ssid>', '02:00:00:00:00:00', '866410035754302', '892daea21c7e035d', 'fe80::fc3f:2aff:fe94:911d%dummy0', 11),
(5, 3918, '02:00:00:00:00:00', '<unknown ssid>', '02:00:00:00:00:00', '866410035754302', '892daea21c7e035d', 'fe80::fc3f:2aff:fe94:911d%dummy0', 11),
(6, 3918, '02:00:00:00:00:00', '<unknown ssid>', '02:00:00:00:00:00', '866410035754302', '892daea21c7e035d', 'fe80::fc3f:2aff:fe94:911d%dummy0', 11),
(7, 3918, '02:00:00:00:00:00', '<unknown ssid>', '02:00:00:00:00:00', '866410035754302', '892daea21c7e035d', 'fe80::34bf:47ff:fea7:62eb%dummy0', 11),
(8, 3918, '02:00:00:00:00:00', '<unknown ssid>', '02:00:00:00:00:00', '866410035754302', '892daea21c7e035d', 'fe80::34bf:47ff:fea7:62eb%dummy0', 11);

-- --------------------------------------------------------

--
-- Table structure for table `branch_mst`
--

CREATE TABLE `branch_mst` (
  `branch_id` int(11) NOT NULL,
  `branch_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch_mst`
--

INSERT INTO `branch_mst` (`branch_id`, `branch_type`) VALUES
(0, 'DTE'),
(1, 'Applied Mechanics'),
(2, 'Archi. Assistantship'),
(3, 'Automobile'),
(4, 'Bio-Medical'),
(5, 'C.A.C.D.D.M.'),
(6, 'Ceramic Technology'),
(7, 'Chemical '),
(8, 'Chemistry'),
(9, 'Civil '),
(10, 'Commercial Practice'),
(11, 'Computer '),
(12, 'D.C.P'),
(13, 'Electronics & Communication'),
(14, 'Electrical '),
(15, 'English'),
(16, 'Environmental'),
(17, 'Fabrication Technology'),
(18, 'Geology'),
(19, 'Home Science'),
(20, 'Industrial'),
(21, 'Information & Technology'),
(22, 'Instrumentation & Control'),
(23, 'Interior Design'),
(24, 'Maths'),
(25, 'Mechanical'),
(26, 'Metallurgy'),
(27, 'Mining '),
(28, 'Physics'),
(29, 'Plastic '),
(30, 'Power Electronics'),
(31, 'Printing Technology'),
(32, 'Production'),
(33, 'Rubber Technology'),
(34, 'Textile Design'),
(35, 'Textile Manu/ Tech'),
(36, 'Textile Proc / Chemistry'),
(37, 'Textile Technology'),
(38, 'Transportation '),
(39, 'Manufacuring'),
(40, 'Administrator'),
(41, 'Induction'),
(42, 'Industrial'),
(43, 'Inter Discipline'),
(44, 'BPharm'),
(45, 'Electronics');

-- --------------------------------------------------------

--
-- Table structure for table `bss_id_mst`
--

CREATE TABLE `bss_id_mst` (
  `b_id` int(11) NOT NULL,
  `bss_id_no` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bss_id_mst`
--

INSERT INTO `bss_id_mst` (`b_id`, `bss_id_no`) VALUES
(1, 'e4:46:da:99:d9:dd'),
(2, '88:79:7e:41:a5:07'),
(22, '78:D3:8D:BD:8B:78'),
(23, '78:D3:8D:BE:9A:C0'),
(24, '345678');

-- --------------------------------------------------------

--
-- Table structure for table `bss_mapping`
--

CREATE TABLE `bss_mapping` (
  `b_m_id` int(11) NOT NULL,
  `b_id` int(11) NOT NULL,
  `inst_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bss_mapping`
--

INSERT INTO `bss_mapping` (`b_m_id`, `b_id`, `inst_id`) VALUES
(1, 1, 25),
(2, 2, 25),
(5, 22, 25),
(6, 23, 25),
(7, 24, 25);

-- --------------------------------------------------------

--
-- Table structure for table `fac_attendance`
--

CREATE TABLE `fac_attendance` (
  `FA_ID` int(11) NOT NULL,
  `LOGINID` int(11) NOT NULL,
  `TDATE` date DEFAULT NULL,
  `ENT_TIME` datetime NOT NULL,
  `EXT_TIME` datetime DEFAULT NULL,
  `STATUS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fac_attendance`
--

INSERT INTO `fac_attendance` (`FA_ID`, `LOGINID`, `TDATE`, `ENT_TIME`, `EXT_TIME`, `STATUS`) VALUES
(35, 1, '2018-10-04', '2018-10-04 15:10:54', '2018-10-04 15:11:18', 11),
(36, 5, '2018-10-04', '2018-10-04 15:12:40', '2018-10-04 15:12:56', 11),
(37, 5, '2018-10-05', '2018-10-05 15:03:37', '2018-10-05 16:02:59', 11),
(38, 10, '2018-10-05', '2018-10-05 15:06:12', NULL, 1),
(39, 1, '2018-10-05', '2018-10-05 15:20:07', '2018-10-05 15:24:49', 11),
(41, 5, '2018-10-06', '2018-10-06 11:06:54', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inst_mst`
--

CREATE TABLE `inst_mst` (
  `inst_id` int(11) NOT NULL,
  `inst_name` varchar(255) DEFAULT NULL,
  `inst_type_id` int(11) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inst_mst`
--

INSERT INTO `inst_mst` (`inst_id`, `inst_name`, `inst_type_id`, `org_id`) VALUES
(1, 'DTE, Gandhinagar', 0, 3),
(2, 'Government Engineering College, Bharuch', 1, 3),
(3, 'Government Engineering College, Bhavnagar', 1, 3),
(4, 'Government Engineering College, BHUJ ', 1, 3),
(5, 'Government Engineering College, Dahod', 1, 3),
(6, 'Government Engineering College, Gandhinagar', 1, 3),
(7, 'Government Engineering College, Godhra', 1, 3),
(8, 'Government Engineering College, Modasa', 1, 3),
(9, 'Government Engineering College, Palanpur', 1, 3),
(10, 'Government Engineering College, Patan', 1, 3),
(11, 'Government Engineering College, Rajkot', 1, 3),
(12, 'Government Engineering College, Surat', 1, 3),
(13, 'Government Engineering College, Valsad', 1, 3),
(14, 'Government MCA College, Maninagar', 1, 3),
(15, 'IITRAM, Ahmedabad', 1, 3),
(16, 'L.D.College of Engineering, Ahmedabad', 1, 3),
(17, 'Lukhdhirji Engineering College, Morbi', 1, 3),
(18, 'Shantilal Shah Engineering College, Bhavnagar', 1, 3),
(19, 'Vishwakarma Govt Engineering College, Chandkheda', 1, 3),
(20, 'AV PAREKH TECHNICAL INSTITUTE, Rajkot', 2, 3),
(21, 'C U Shah Polytechnic, SuredraNagar', 2, 3),
(22, 'Dr. S & S. S. Ghandhy College of Engineering & Technology, Surat', 2, 3),
(23, 'Government Polytechnic For Girls, Ahmedabad', 2, 3),
(24, 'Government Polytechnic For Girls, Surat', 2, 3),
(25, 'Government Polytechnic, Ahmedabad', 2, 3),
(26, 'Government Polytechnic, Bhuj', 2, 3),
(27, 'Government Polytechnic, chhotaudepur', 2, 3),
(28, 'Government Polytechnic, Dahod', 2, 3),
(29, 'Government Polytechnic, Gandhinagar', 2, 3),
(30, 'Government Polytechnic, Godhra', 2, 3),
(31, 'Government Polytechnic, Himatnagar', 2, 3),
(32, 'Government Polytechnic, Jamnagar', 2, 3),
(33, 'Government Polytechnic, Junagadh', 2, 3),
(34, 'Government Polytechnic, Kheda', 2, 3),
(35, 'Government Polytechnic, Navsari', 2, 3),
(36, 'Government Polytechnic, Palanpur', 2, 3),
(37, 'Government Polytechnic, Porbandar', 2, 3),
(38, 'Government Polytechnic, Rajkot', 2, 3),
(39, 'Government Polytechnic, Rajpipla', 2, 3),
(40, 'Government Polytechnic, Vadnagar', 2, 3),
(41, 'Government Polytechnic, VALSAD', 2, 3),
(42, 'Government Polytechnic, Vyara', 2, 3),
(43, 'Government Polytechnic, Waghai', 2, 3),
(44, 'JNMP, Amreli', 2, 3),
(45, 'K D Polytechnic, Patan', 2, 3),
(46, 'LUKHDHIRJI ENGINEERING COLLEGE (DIPLOMA), Morbi', 2, 3),
(47, 'RC TECHNICAL INSTITUTE, Ahmedabad', 2, 3),
(48, 'Shri K J Polytechnic, Bharuch', 2, 3),
(49, 'Sir Bhavsinhji Polytechnic Institute, Bhavnagar', 2, 3),
(50, 'Indian Institute of Science, Bengaluru', 5, 1),
(51, 'Indian Institute of Technology (IIT), Gandhi Nagar', 5, 1),
(52, 'Indian Institute of Technology (IIT), Bhubaneshwar', 5, 1),
(53, 'Indian Institute of Technology (IIT), Madras', 5, 1),
(54, 'Indian Institute of Technology (IIT), Guwahati', 5, 1),
(55, 'Indian Institute of Technology (IIT), Indore', 5, 1),
(56, 'Indian Institute of Technology (IIT), Kanpur', 5, 1),
(57, 'Indian Institute of Technology (IIT), Jodhpur', 5, 1),
(58, 'Indian Institute of Technology (IIT), Kharagpur', 5, 1),
(59, 'Indian Institute of Technology (IIT), Hyderabad', 5, 1),
(60, 'Indian Institute of Technology (IIT), Mumbai', 5, 1),
(61, 'Indian Institute of Technology (IIT), Patna', 5, 1),
(62, 'Indian Institute of Technology (IIT), Delhi', 5, 1),
(63, 'Indian Institute of Technology (IIT), Ropar', 5, 1),
(64, 'Indian Institute of Technology (IIT), Mandi', 5, 1),
(65, 'Indian Institute of Technology (IIT), Roorkee', 5, 1),
(66, 'Indian Institute of Technology (Banaras Hindu University), Varanasi', 5, 1),
(67, 'Indian Institute of Technology (IIT), Jammu', 5, 1),
(68, 'Indian Institute of Technology (IIT), Palakkad', 5, 1),
(69, 'Indian Institute of Technology (IIT), Tirupati', 5, 1),
(70, 'Indian Institute of Technology (IIT), Goa', 5, 1),
(71, 'Indian Institute of Technology (IIT), Bhilai', 5, 1),
(72, 'Indian Institute of Technology (IIT) Dharwad', 5, 1),
(73, 'Indian Institute of Technology (Indian School of Mines), Dhanbad', 5, 1),
(74, 'Indian Institute of Management, Ahmedabad, Gujarat', 5, 1),
(75, 'Indian Institute of Management, Kolkata, West Bengal', 5, 1),
(76, 'Indian Institute of Management, Bangalore, Karnataka', 5, 1),
(77, 'Indian Institute of Management, Lucknow, Uttar Pradesh', 5, 1),
(78, 'Indian Institute of Management, Kozhikode, Kerala', 5, 1),
(79, 'Indian Institute of Management, Indore, Madhya Pradesh', 5, 1),
(80, 'Indian Institute of Management, Shillong, Meghalaya', 5, 1),
(81, 'Indian Institute of Management, Rohtak, Haryana', 5, 1),
(82, 'Indian Institute of Management, Ranchi, Jharkhand', 5, 1),
(83, 'Indian Institute of Management, Raipur, Chhattisgarh', 5, 1),
(84, 'Indian Institute of Management, Tiruchirappalli, Tamil Nadu', 5, 1),
(85, 'Indian Institute of Management, Kashipur, Uttarakhand', 5, 1),
(86, 'Indian Institute of Management, Udaipur, Rajasthan', 5, 1),
(87, 'Indian Institute of Management, Nagpur, Maharashtra', 5, 1),
(88, 'Indian Institute of Management, Vishakhapatnam, Andhra Pradesh', 5, 1),
(89, 'Indian Institute of Management, Bodh Gaya, Bihar', 5, 1),
(90, 'Indian Institute of Management, Amritsar, Punjab', 5, 1),
(91, 'Indian Institute of Management, Sambalpur, Odisha', 5, 1),
(92, 'Indian Institute of Management, Sirmaur, Himachal Pradesh', 5, 1),
(93, 'Indian Institute of Management, Jammu, Jammu & Kashmir', 5, 1),
(94, 'Ahmedabad University, Ahmedabad', 5, 2),
(95, 'Anand Agricultural University, anand', 5, 2),
(96, 'Anant National University, Ahmedabad', 5, 2),
(97, 'AURO University of Hospitality and Management, Surat', 5, 2),
(98, 'Bhakta Kavi Narsinh Mehta University, Junagadh', 5, 2),
(99, 'C. U. Shah University, Surendranagar', 5, 2),
(100, 'Calorx Teacher\'s University, Ahmadabad', 5, 2),
(101, 'Central University of Gujarat, Gandhinagar', 5, 2),
(102, 'CEPT University, Ahmedabad', 5, 2),
(103, 'Charotar University of Science & Technology, Anand', 5, 2),
(104, 'Children\'s University, Gandhinagar', 5, 2),
(105, 'Dharmsinh Desai University, Nadiad', 5, 2),
(106, 'Dhirubhai Ambani Institute of Information and Communication Technology, Gandhinagar', 5, 2),
(107, 'Dr. Babasaheb Ambedkar Open University, Ahmedabad', 5, 2),
(108, 'G.L.S. University, Ahmedabad', 5, 2),
(109, 'Ganpat University, Mehsana', 5, 2),
(110, 'GSFC University, Vadodara', 5, 2),
(111, 'Gujarat Agricultural University, Banaskantha', 5, 2),
(112, 'Gujarat Ayurveda University, Jamnagar', 5, 2),
(113, 'Gujarat Forensic Sciences University, Gandhinagar', 5, 2),
(114, 'Gujarat National Law University, Gandhinagar', 5, 2),
(115, 'Gujarat Technological University, Ahmedabad', 5, 2),
(116, 'Gujarat University of Transplantation Sciences, Ahmedabad', 5, 2),
(117, 'Gujarat University, Ahmedabad', 5, 2),
(118, 'Gujarat Vidyapeeth, Ahmedabad', 5, 2),
(119, 'Hemchandracharya North Gujarat University, Patan', 5, 2),
(120, 'Indian Institute of Public Health, Ahmedabad', 5, 2),
(121, 'Indian Institute of Teacher Education, Gandhinagar', 5, 2),
(122, 'Indrashil University, Ahmedabad', 5, 2),
(123, 'Indus University, Ahmedabad', 5, 2),
(124, 'Institute of Advanced Research, Gandhinagar', 5, 2),
(125, 'Institute of Infrastructure Technology Research and Management, Ahmedabad', 5, 2),
(126, 'ITM Vocational University, Vadodara', 5, 2),
(127, 'Junagarh Agricultural University, Junagadh', 5, 2),
(128, 'Kadi Sarva Vishwavidyalaya, Gandhinagar', 5, 2),
(129, 'Kamdhenu University, Gandhinagar', 5, 2),
(130, 'Karnavati University, Gandhinagar', 5, 2),
(131, 'Krantiguru Shyamji Krishna Verma Kachchh University, Kachchh', 5, 2),
(132, 'Krishnakumarsinhji Bhavnagar University, Bhavnagar', 5, 2),
(133, 'Lakulish Yoga University, Ahmedabad', 5, 2),
(134, 'Maharaja Sayajirao University of Baroda, Vadodara', 5, 2),
(135, 'Marwadi University, Rajkot', 5, 2),
(136, 'Navrachana University, Ahmedabad', 5, 2),
(137, 'Navsari Agriculture University, Navsari', 5, 2),
(138, 'Nirma University, Ahmedabad', 5, 2),
(139, 'P. P. Savani University, Surat', 5, 2),
(140, 'Pandit Deendayal petroleum University, Gandhinagar', 5, 2),
(141, 'Parul University, Vadodara', 5, 2),
(142, 'Plastindia International University, Valsad', 5, 2),
(143, 'Rai University, Ahmedabad', 5, 2),
(144, 'Raksha Shakti University, Ahmedabad', 5, 2),
(145, 'RK University, Rajkot', 5, 2),
(146, 'Sankalchand Patel University, Visanagar', 5, 2),
(147, 'Sardar Patel University, Anand', 5, 2),
(148, 'Saurashtra University, Rajkot', 5, 2),
(149, 'Shree Somnath Sanskrit University, Junagadh', 5, 2),
(150, 'Shri Govind Guru University, Panchmahals', 5, 2),
(151, 'Sumandeep Vidyapeeth, Vadodara', 5, 2),
(152, 'Swarnim Gujarat Sports University, Gandhinagar', 5, 2),
(153, 'Swarnim Startup & Innovation University, Gandhinagar', 5, 2),
(154, 'Team Lease Skills University, Vadodara', 5, 2),
(155, 'UKA Tarsadia University, Surat', 5, 2),
(156, 'Veer Narmad South Gujarat University, Surat', 5, 2),
(157, 'SPIPA', 5, 4),
(158, 'NITTTR,Ahmedabad', 5, 6),
(159, 'NITTTR,Bhopal', 5, 6),
(160, 'ALISON', 5, 7),
(161, 'Canvas Network', 5, 7),
(162, 'Coursera', 5, 7),
(163, 'Coursmos', 5, 7),
(164, 'edX', 5, 7),
(165, 'Eliademy', 5, 7),
(166, 'FutureLearn', 5, 7),
(167, 'iversity', 5, 7),
(168, 'Kadenze', 5, 7),
(169, 'Khan Academy', 5, 7),
(170, 'Lynda.com', 5, 7),
(171, 'NovoEd', 5, 7),
(172, 'Open2Study', 5, 7),
(173, 'OpenClassrooms', 5, 7),
(174, 'openHPI', 5, 7),
(175, 'OpenLearning', 5, 7),
(176, 'Peer to Peer University', 5, 7),
(177, 'POLHN', 5, 7),
(178, 'Shaw Academy', 5, 7),
(179, 'Stanford Online', 5, 7),
(180, 'Udacity', 5, 7),
(181, 'Udemy', 5, 7),
(182, 'WizIQ', 5, 7),
(183, 'Motilal Nehru National Institute of Technology Allahabad', 5, 1),
(184, 'National Institute of Technology Sikkim', 5, 1),
(185, 'National Institute of Technology Patna', 5, 1),
(186, 'National Institute of Technology Karnataka, Surathkal', 5, 1),
(187, 'Malaviya National Institute of Technology Jaipur', 5, 1),
(188, 'National Institute of Technology, Uttarakhand', 5, 1),
(189, 'National Institute of Technology Durgapur', 5, 1),
(190, 'National Institute of Technology Warangal', 5, 1),
(191, 'National Institute of Technology Rourkela', 5, 1),
(192, 'National Institute of Technology Manipur', 5, 1),
(193, 'National Institute of Technology Kurukshetra', 5, 1),
(194, 'National Institute of Technology, Hamirpur', 5, 1),
(195, 'Visvesvaraya National Institute of Technology Nagpur', 5, 1),
(196, 'Sardar Vallabhbhai National Institute of Technology Surat', 5, 1),
(197, 'National Institute of Technology Tiruchirappalli', 5, 1),
(198, 'National Institute of Technology Srinagar', 5, 1),
(199, 'National Institute of Technology Mizoram', 5, 1),
(200, 'National Institute of Technology Meghalaya', 5, 1),
(201, 'National Institute of Technology Jamshedpur', 5, 1),
(202, 'National Institute of Technology Goa', 5, 1),
(203, 'National Institute of Technology Delhi', 5, 1),
(204, 'National Institute of Technology Calicut', 5, 1),
(205, 'National Institute of Technology Arunachal Pradesh', 5, 1),
(206, 'National Institute of Technology Agartala', 5, 1),
(207, 'National Institute of Technology Andhra Pradesh', 5, 1),
(208, 'Maulana Azad National Institute of Technology Bhopal', 5, 1),
(209, 'Dr B R Ambedkar National Institute of Technology Jalandhar', 5, 1),
(210, 'National Institute of Technology Puducherry, Karaikal', 5, 1),
(211, 'National Institute of Technology, Raipur', 5, 1),
(212, 'National Institute of Technology, Silchar', 5, 1),
(213, 'NITTTR,Goa', 5, 6),
(214, 'NITTTR,Raipur', 5, 6),
(215, 'NITTTR,Pune', 5, 6),
(216, 'NITTTR,chandigarh', 5, 6),
(217, 'NITTTR,kolkata', 5, 6),
(218, 'NITTTR,Chennai', 5, 6),
(219, 'B.K.Mody Government Pharmacy College,Rajkot', 3, 3),
(220, 'NPTEL', 5, 7),
(221, 'BVM Engineering College,Vallabh Vidyanagar', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `inst_type_mst`
--

CREATE TABLE `inst_type_mst` (
  `inst_type_id` int(11) NOT NULL,
  `inst_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inst_type_mst`
--

INSERT INTO `inst_type_mst` (`inst_type_id`, `inst_type`) VALUES
(0, 'DTE,Gandhinagar'),
(1, 'Degree Engineering'),
(2, 'Diploma Engineering'),
(3, 'Degree Pharmacy'),
(4, 'Diploma Pharmacy'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `LOGINID` int(11) NOT NULL,
  `LID` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `Reg_Id` varchar(255) DEFAULT NULL,
  `Reset_Count` int(11) DEFAULT NULL,
  `ResetBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`LOGINID`, `LID`, `PASSWORD`, `Reg_Id`, `Reset_Count`, `ResetBy`) VALUES
(1, 'chetanbb', '12345', '2479', 0, 0),
(2, 'jagdishtp', '12345', '2535', 0, 0),
(3, 'shreejivg', '12345', '2528', 0, 0),
(4, 'parthhm', '12345', '2534', 0, 0),
(5, 'Bhavya', '12345', '3918', 0, 0),
(6, 'Jyoti', '12345', '1', 0, 0),
(7, 'vishal', '12345', '12', 0, 0),
(8, 'vatsal', '12345', '2', 0, 0),
(9, 'purvi', 'pur123', '4', 0, 0),
(10, 'bhavesh', 'bha123', '2', 0, 0),
(12, 'girish', 'gir123', '3', 0, 0),
(13, 'payal', 'pay123', '118', 0, 0),
(14, 'dixit', 'dix123', '117', 0, 0),
(15, 'tejash', 'tej123', '119', 0, 0),
(16, 'bharat', 'bha123', '120', 0, 0),
(17, 'foram', 'for123', '121', 0, 0),
(18, 'darshana', 'dar123', '122', 0, 0),
(19, 'falguni', 'fal123', '123', 0, 0),
(20, 'rupal', 'rup123', '124', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `map_inst_branch`
--

CREATE TABLE `map_inst_branch` (
  `map_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `inst_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `map_inst_branch`
--

INSERT INTO `map_inst_branch` (`map_id`, `branch_id`, `branch_name`, `inst_id`) VALUES
(1, 0, 'DTE', 1),
(2, 1, 'Applied Mechanics', 2),
(3, 7, 'Chemical ', 2),
(4, 8, 'Chemistry', 2),
(5, 9, 'Civil ', 2),
(6, 14, 'Electrical ', 2),
(7, 13, 'Electronics & Communication', 2),
(8, 15, 'English', 2),
(9, 24, 'Maths', 2),
(10, 25, 'Mechanical', 2),
(11, 28, 'Physics', 2),
(12, 1, 'Applied Mechanics', 3),
(13, 9, 'Civil ', 3),
(14, 11, 'Computer ', 3),
(15, 14, 'Electrical ', 3),
(16, 13, 'Electronics & Communication', 3),
(17, 15, 'English', 3),
(18, 21, 'Information & Technology', 3),
(19, 24, 'Maths', 3),
(20, 25, 'Mechanical', 3),
(21, 28, 'Physics', 3),
(22, 32, 'Production', 3),
(23, 1, 'Applied Mechanics', 4),
(24, 7, 'Chemical ', 4),
(25, 8, 'Chemistry', 4),
(26, 9, 'Civil ', 4),
(27, 11, 'Computer ', 4),
(28, 14, 'Electrical ', 4),
(29, 13, 'Electronics & Communication', 4),
(30, 15, 'English', 4),
(31, 16, 'Environmental', 4),
(32, 18, 'Geology', 4),
(33, 24, 'Maths', 4),
(34, 25, 'Mechanical', 4),
(35, 27, 'Mining ', 4),
(36, 28, 'Physics', 4),
(37, 1, 'Applied Mechanics', 5),
(38, 9, 'Civil ', 5),
(39, 11, 'Computer ', 5),
(40, 14, 'Electrical ', 5),
(41, 13, 'Electronics & Communication', 5),
(42, 15, 'English', 5),
(43, 25, 'Mechanical', 5),
(44, 1, 'Applied Mechanics', 6),
(45, 4, 'Bio-Medical', 6),
(46, 9, 'Civil ', 6),
(47, 11, 'Computer ', 6),
(48, 14, 'Electrical ', 6),
(49, 13, 'Electronics & Communication', 6),
(50, 15, 'English', 6),
(51, 21, 'Information & Technology', 6),
(52, 22, 'Instrumentation & Control', 6),
(53, 24, 'Maths', 6),
(54, 25, 'Mechanical', 6),
(55, 26, 'Metallurgy', 6),
(56, 28, 'Physics', 6),
(57, 1, 'Applied Mechanics', 7),
(58, 9, 'Civil ', 7),
(59, 14, 'Electrical ', 7),
(60, 13, 'Electronics & Communication', 7),
(61, 15, 'English', 7),
(62, 24, 'Maths', 7),
(63, 25, 'Mechanical', 7),
(64, 28, 'Physics', 7),
(65, 1, 'Applied Mechanics', 8),
(66, 3, 'Automobile', 8),
(67, 9, 'Civil ', 8),
(68, 11, 'Computer ', 8),
(69, 14, 'Electrical ', 8),
(70, 13, 'Electronics & Communication', 8),
(71, 15, 'English', 8),
(72, 21, 'Information & Technology', 8),
(73, 24, 'Maths', 8),
(74, 25, 'Mechanical', 8),
(75, 28, 'Physics', 8),
(76, 1, 'Applied Mechanics', 9),
(77, 8, 'Chemistry', 9),
(78, 9, 'Civil ', 9),
(79, 14, 'Electrical ', 9),
(80, 18, 'Geology', 9),
(81, 24, 'Maths', 9),
(82, 25, 'Mechanical', 9),
(83, 27, 'Mining ', 9),
(84, 28, 'Physics', 9),
(85, 1, 'Applied Mechanics', 10),
(86, 9, 'Civil ', 10),
(87, 11, 'Computer ', 10),
(88, 14, 'Electrical ', 10),
(89, 13, 'Electronics & Communication', 10),
(90, 15, 'English', 10),
(91, 24, 'Maths', 10),
(92, 25, 'Mechanical', 10),
(93, 28, 'Physics', 10),
(94, 1, 'Applied Mechanics', 11),
(95, 3, 'Automobile', 11),
(96, 9, 'Civil ', 11),
(97, 11, 'Computer ', 11),
(98, 12, 'D.C.P', 11),
(99, 14, 'Electrical ', 11),
(100, 13, 'Electronics & Communication', 11),
(101, 15, 'English', 11),
(102, 22, 'Instrumentation & Control', 11),
(103, 24, 'Maths', 11),
(104, 25, 'Mechanical', 11),
(105, 28, 'Physics', 11),
(106, 1, 'Applied Mechanics', 12),
(107, 9, 'Civil ', 12),
(108, 11, 'Computer ', 12),
(109, 14, 'Electrical ', 12),
(110, 13, 'Electronics & Communication', 12),
(111, 15, 'English', 12),
(112, 16, 'Environmental', 12),
(113, 24, 'Maths', 12),
(114, 25, 'Mechanical', 12),
(115, 28, 'Physics', 12),
(116, 1, 'Applied Mechanics', 13),
(117, 7, 'Chemical ', 13),
(118, 8, 'Chemistry', 13),
(119, 9, 'Civil ', 13),
(120, 14, 'Electrical ', 13),
(121, 13, 'Electronics & Communication', 13),
(122, 15, 'English', 13),
(123, 16, 'Environmental', 13),
(124, 24, 'Maths', 13),
(125, 25, 'Mechanical', 13),
(126, 28, 'Physics', 13),
(127, 22, 'Instrumentation & Control', 14),
(128, 9, 'Civil', 15),
(129, 1, 'Applied Mechanics', 16),
(130, 3, 'Automobile', 16),
(131, 4, 'Bio-Medical', 16),
(132, 7, 'Chemical ', 16),
(133, 8, 'Chemistry', 16),
(134, 9, 'Civil ', 16),
(135, 11, 'Computer ', 16),
(136, 14, 'Electrical ', 16),
(137, 13, 'Electronics & Communication', 16),
(138, 15, 'English', 16),
(139, 16, 'Environmental', 16),
(140, 18, 'Geology', 16),
(141, 21, 'Information & Technology', 16),
(142, 22, 'Instrumentation & Control', 16),
(143, 24, 'Maths', 16),
(144, 25, 'Mechanical', 16),
(145, 26, 'Metallurgy', 16),
(146, 28, 'Physics', 16),
(147, 29, 'Plastic ', 16),
(148, 33, 'Rubber Technology', 16),
(149, 37, 'Textile Technology', 16),
(150, 1, 'Applied Mechanics', 17),
(151, 7, 'Chemical ', 17),
(152, 8, 'Chemistry', 17),
(153, 9, 'Civil ', 17),
(154, 14, 'Electrical ', 17),
(155, 15, 'English', 17),
(156, 20, 'Industrial', 17),
(157, 21, 'Information & Technology', 17),
(158, 24, 'Maths', 17),
(159, 25, 'Mechanical', 17),
(160, 28, 'Physics', 17),
(161, 30, 'Power Electronics', 17),
(162, 32, 'Production', 17),
(163, 1, 'Applied Mechanics', 18),
(164, 9, 'Civil ', 18),
(165, 14, 'Electrical ', 18),
(166, 13, 'Electronics & Communication', 18),
(167, 15, 'English', 18),
(168, 20, 'Industrial', 18),
(169, 21, 'Information & Technology', 18),
(170, 22, 'Instrumentation & Control', 18),
(171, 24, 'Maths', 18),
(172, 25, 'Mechanical', 18),
(173, 28, 'Physics', 18),
(174, 32, 'Production', 18),
(175, 1, 'Applied Mechanics', 19),
(176, 7, 'Chemical ', 19),
(177, 8, 'Chemistry', 19),
(178, 9, 'Civil ', 19),
(179, 11, 'Computer ', 19),
(180, 14, 'Electrical ', 19),
(181, 13, 'Electronics & Communication', 19),
(182, 15, 'English', 19),
(183, 21, 'Information & Technology', 19),
(184, 22, 'Instrumentation & Control', 19),
(185, 24, 'Maths', 19),
(186, 25, 'Mechanical', 19),
(187, 28, 'Physics', 19),
(188, 30, 'Power Electronics', 19),
(189, 4, 'Bio-Medical', 20),
(190, 5, 'C.A.C.D.D.M.', 20),
(191, 8, 'Chemistry', 20),
(192, 10, 'Commercial Practice', 20),
(193, 11, 'Computer ', 20),
(194, 14, 'Electrical ', 20),
(195, 13, 'Electronics & Communication', 20),
(196, 15, 'English', 20),
(197, 19, 'Home Science', 20),
(198, 22, 'Instrumentation & Control', 20),
(199, 24, 'Maths', 20),
(200, 25, 'Mechanical', 20),
(201, 28, 'Physics', 20),
(202, 1, 'Applied Mechanics', 21),
(203, 3, 'Automobile', 21),
(204, 5, 'C.A.C.D.D.M.', 21),
(205, 8, 'Chemistry', 21),
(206, 9, 'Civil ', 21),
(207, 11, 'Computer ', 21),
(208, 14, 'Electrical ', 21),
(209, 15, 'English', 21),
(210, 19, 'Home Science', 21),
(211, 24, 'Maths', 21),
(212, 25, 'Mechanical', 21),
(213, 28, 'Physics', 21),
(214, 1, 'Applied Mechanics', 22),
(215, 3, 'Automobile', 22),
(216, 8, 'Chemistry', 22),
(217, 9, 'Civil ', 22),
(218, 14, 'Electrical ', 22),
(219, 15, 'English', 22),
(220, 21, 'Information & Technology', 22),
(221, 24, 'Maths', 22),
(222, 25, 'Mechanical', 22),
(223, 26, 'Metallurgy', 22),
(224, 28, 'Physics', 22),
(225, 30, 'Power Electronics', 22),
(226, 35, 'Textile Manu/ Tech', 22),
(227, 36, 'Textile Proc / Chemistry', 22),
(228, 1, 'Applied Mechanics', 23),
(229, 2, 'Archi. Assistantship', 23),
(230, 4, 'Bio-Medical', 23),
(231, 5, 'C.A.C.D.D.M.', 23),
(232, 9, 'Civil ', 23),
(233, 10, 'Commercial Practice', 23),
(234, 11, 'Computer ', 23),
(235, 14, 'Electrical ', 23),
(236, 13, 'Electronics & Communication', 23),
(237, 15, 'English', 23),
(238, 19, 'Home Science', 23),
(239, 21, 'Information & Technology', 23),
(240, 23, 'Interior Design', 23),
(241, 24, 'Maths', 23),
(242, 25, 'Mechanical', 23),
(243, 28, 'Physics', 23),
(244, 2, 'Archi. Assistantship', 24),
(245, 9, 'Civil ', 24),
(246, 10, 'Commercial Practice', 24),
(247, 11, 'Computer ', 24),
(248, 14, 'Electrical ', 24),
(249, 13, 'Electronics & Communication', 24),
(250, 15, 'English', 24),
(251, 21, 'Information & Technology', 24),
(252, 24, 'Maths', 24),
(253, 25, 'Mechanical', 24),
(254, 28, 'Physics', 24),
(255, 34, 'Textile Design', 24),
(256, 1, 'Applied Mechanics', 25),
(257, 3, 'Automobile', 25),
(258, 4, 'Bio-Medical', 25),
(259, 8, 'Chemistry', 25),
(260, 9, 'Civil ', 25),
(261, 11, 'Computer ', 25),
(262, 14, 'Electrical ', 25),
(263, 13, 'Electronics & Communication', 25),
(264, 15, 'English', 25),
(265, 21, 'Information & Technology', 25),
(266, 22, 'Instrumentation & Control', 25),
(267, 24, 'Maths', 25),
(268, 25, 'Mechanical', 25),
(269, 28, 'Physics', 25),
(270, 29, 'Plastic ', 25),
(271, 1, 'Applied Mechanics', 26),
(272, 8, 'Chemistry', 26),
(273, 9, 'Civil ', 26),
(274, 11, 'Computer ', 26),
(275, 14, 'Electrical ', 26),
(276, 15, 'English', 26),
(277, 21, 'Information & Technology', 26),
(278, 24, 'Maths', 26),
(279, 25, 'Mechanical', 26),
(280, 26, 'Metallurgy', 26),
(281, 27, 'Mining ', 26),
(282, 28, 'Physics', 26),
(283, 1, 'Applied Mechanics', 27),
(284, 3, 'Automobile', 27),
(285, 8, 'Chemistry', 27),
(286, 9, 'Civil ', 27),
(287, 14, 'Electrical ', 27),
(288, 15, 'English', 27),
(289, 24, 'Maths', 27),
(290, 25, 'Mechanical', 27),
(291, 28, 'Physics', 27),
(292, 29, 'Plastic ', 27),
(293, 1, 'Applied Mechanics', 28),
(294, 5, 'C.A.C.D.D.M.', 28),
(295, 9, 'Civil ', 28),
(296, 11, 'Computer ', 28),
(297, 14, 'Electrical ', 28),
(298, 13, 'Electronics & Communication', 28),
(299, 15, 'English', 28),
(300, 24, 'Maths', 28),
(301, 25, 'Mechanical', 28),
(302, 28, 'Physics', 28),
(303, 5, 'C.A.C.D.D.M.', 29),
(304, 7, 'Chemical ', 29),
(305, 8, 'Chemistry', 29),
(306, 9, 'Civil ', 29),
(307, 11, 'Computer ', 29),
(308, 14, 'Electrical ', 29),
(309, 13, 'Electronics & Communication', 29),
(310, 15, 'English', 29),
(311, 21, 'Information & Technology', 29),
(312, 22, 'Instrumentation & Control', 29),
(313, 24, 'Maths', 29),
(314, 25, 'Mechanical', 29),
(315, 28, 'Physics', 29),
(316, 1, 'Applied Mechanics', 30),
(317, 8, 'Chemistry', 30),
(318, 9, 'Civil ', 30),
(319, 14, 'Electrical ', 30),
(320, 15, 'English', 30),
(321, 19, 'Home Science', 30),
(322, 24, 'Maths', 30),
(323, 25, 'Mechanical', 30),
(324, 28, 'Physics', 30),
(325, 1, 'Applied Mechanics', 31),
(326, 5, 'C.A.C.D.D.M.', 31),
(327, 8, 'Chemistry', 31),
(328, 9, 'Civil ', 31),
(329, 11, 'Computer ', 31),
(330, 14, 'Electrical ', 31),
(331, 13, 'Electronics & Communication', 31),
(332, 15, 'English', 31),
(333, 21, 'Information & Technology', 31),
(334, 24, 'Maths', 31),
(335, 25, 'Mechanical', 31),
(336, 28, 'Physics', 31),
(337, 1, 'Applied Mechanics', 32),
(338, 8, 'Chemistry', 32),
(339, 9, 'Civil ', 32),
(340, 11, 'Computer ', 32),
(341, 14, 'Electrical ', 32),
(342, 13, 'Electronics & Communication', 32),
(343, 15, 'English', 32),
(344, 24, 'Maths', 32),
(345, 25, 'Mechanical', 32),
(346, 28, 'Physics', 32),
(347, 1, 'Applied Mechanics', 33),
(348, 8, 'Chemistry', 33),
(349, 9, 'Civil ', 33),
(350, 14, 'Electrical ', 33),
(351, 15, 'English', 33),
(352, 24, 'Maths', 33),
(353, 25, 'Mechanical', 33),
(354, 28, 'Physics', 33),
(355, 1, 'Applied Mechanics', 34),
(356, 9, 'Civil ', 34),
(357, 14, 'Electrical ', 34),
(358, 15, 'English', 34),
(359, 24, 'Maths', 34),
(360, 25, 'Mechanical', 34),
(361, 28, 'Physics', 34),
(362, 1, 'Applied Mechanics', 35),
(363, 8, 'Chemistry', 35),
(364, 9, 'Civil ', 35),
(365, 14, 'Electrical ', 35),
(366, 15, 'English', 35),
(367, 24, 'Maths', 35),
(368, 25, 'Mechanical', 35),
(369, 28, 'Physics', 35),
(370, 1, 'Applied Mechanics', 36),
(371, 8, 'Chemistry', 36),
(372, 9, 'Civil ', 36),
(373, 14, 'Electrical ', 36),
(374, 13, 'Electronics & Communication', 36),
(375, 15, 'English', 36),
(376, 22, 'Instrumentation & Control', 36),
(377, 24, 'Maths', 36),
(378, 25, 'Mechanical', 36),
(379, 28, 'Physics', 36),
(380, 1, 'Applied Mechanics', 37),
(381, 9, 'Civil ', 37),
(382, 11, 'Computer ', 37),
(383, 14, 'Electrical ', 37),
(384, 15, 'English', 37),
(385, 24, 'Maths', 37),
(386, 25, 'Mechanical', 37),
(387, 28, 'Physics', 37),
(388, 38, 'Transportation ', 37),
(389, 1, 'Applied Mechanics', 38),
(390, 7, 'Chemical ', 38),
(391, 8, 'Chemistry', 38),
(392, 9, 'Civil ', 38),
(393, 10, 'Commercial Practice', 38),
(394, 11, 'Computer ', 38),
(395, 14, 'Electrical ', 38),
(396, 13, 'Electronics & Communication', 38),
(397, 15, 'English', 38),
(398, 21, 'Information & Technology', 38),
(399, 24, 'Maths', 38),
(400, 25, 'Mechanical', 38),
(401, 26, 'Metallurgy', 38),
(402, 28, 'Physics', 38),
(403, 14, 'Electrical ', 39),
(404, 25, 'Mechanical', 39),
(405, 2, 'Archi. Assistantship', 40),
(406, 9, 'Civil ', 40),
(407, 13, 'Electronics & Communication', 40),
(408, 15, 'English', 40),
(409, 24, 'Maths', 40),
(410, 25, 'Mechanical', 40),
(411, 28, 'Physics', 40),
(412, 1, 'Applied Mechanics', 41),
(413, 7, 'Chemical ', 41),
(414, 8, 'Chemistry', 41),
(415, 9, 'Civil ', 41),
(416, 14, 'Electrical ', 41),
(417, 13, 'Electronics & Communication', 41),
(418, 15, 'English', 41),
(419, 24, 'Maths', 41),
(420, 25, 'Mechanical', 41),
(421, 28, 'Physics', 41),
(422, 29, 'Plastic ', 41),
(423, 1, 'Applied Mechanics', 42),
(424, 8, 'Chemistry', 42),
(425, 9, 'Civil ', 42),
(426, 14, 'Electrical ', 42),
(427, 15, 'English', 42),
(428, 25, 'Mechanical', 42),
(429, 28, 'Physics', 42),
(430, 1, 'Applied Mechanics', 43),
(431, 8, 'Chemistry', 43),
(432, 9, 'Civil ', 43),
(433, 11, 'Computer ', 43),
(434, 14, 'Electrical ', 43),
(435, 15, 'English', 43),
(436, 21, 'Information & Technology', 43),
(437, 24, 'Maths', 43),
(438, 25, 'Mechanical', 43),
(439, 28, 'Physics', 43),
(440, 1, 'Applied Mechanics', 44),
(441, 3, 'Automobile', 44),
(442, 8, 'Chemistry', 44),
(443, 9, 'Civil ', 44),
(444, 14, 'Electrical ', 44),
(445, 13, 'Electronics & Communication', 44),
(446, 15, 'English', 44),
(447, 24, 'Maths', 44),
(448, 25, 'Mechanical', 44),
(449, 28, 'Physics', 44),
(450, 1, 'Applied Mechanics', 45),
(451, 8, 'Chemistry', 45),
(452, 9, 'Civil ', 45),
(453, 11, 'Computer ', 45),
(454, 14, 'Electrical ', 45),
(455, 13, 'Electronics & Communication', 45),
(456, 15, 'English', 45),
(457, 24, 'Maths', 45),
(458, 25, 'Mechanical', 45),
(459, 28, 'Physics', 45),
(460, 1, 'Applied Mechanics', 46),
(461, 6, 'Ceramic Technology', 46),
(462, 8, 'Chemistry', 46),
(463, 9, 'Civil ', 46),
(464, 14, 'Electrical ', 46),
(465, 13, 'Electronics & Communication', 46),
(466, 15, 'English', 46),
(467, 21, 'Information & Technology', 46),
(468, 24, 'Maths', 46),
(469, 25, 'Mechanical', 46),
(470, 26, 'Metallurgy', 46),
(471, 28, 'Physics', 46),
(472, 1, 'Applied Mechanics', 47),
(473, 8, 'Chemistry', 47),
(474, 9, 'Civil ', 47),
(475, 11, 'Computer ', 47),
(476, 14, 'Electrical ', 47),
(477, 15, 'English', 47),
(478, 21, 'Information & Technology', 47),
(479, 24, 'Maths', 47),
(480, 25, 'Mechanical', 47),
(481, 28, 'Physics', 47),
(482, 31, 'Printing Technology', 47),
(483, 35, 'Textile Manu/ Tech', 47),
(484, 36, 'Textile Proc / Chemistry', 47),
(485, 1, 'Applied Mechanics', 48),
(486, 5, 'C.A.C.D.D.M.', 48),
(487, 7, 'Chemical ', 48),
(488, 9, 'Civil ', 48),
(489, 11, 'Computer ', 48),
(490, 14, 'Electrical ', 48),
(491, 13, 'Electronics & Communication', 48),
(492, 15, 'English', 48),
(493, 16, 'Environmental', 48),
(494, 19, 'Home Science', 48),
(495, 24, 'Maths', 48),
(496, 25, 'Mechanical', 48),
(497, 28, 'Physics', 48),
(498, 1, 'Applied Mechanics', 49),
(499, 3, 'Automobile', 49),
(500, 5, 'C.A.C.D.D.M.', 49),
(501, 7, 'Chemical ', 49),
(502, 9, 'Civil ', 49),
(503, 10, 'Commercial Practice', 49),
(504, 14, 'Electrical ', 49),
(505, 13, 'Electronics & Communication', 49),
(506, 15, 'English', 49),
(507, 17, 'Fabrication Technology', 49),
(508, 21, 'Information & Technology', 49),
(509, 24, 'Maths', 49),
(510, 25, 'Mechanical', 49),
(511, 28, 'Physics', 49),
(512, 35, 'Textile Manu/ Tech', 49),
(513, 14, 'Electrical', 15),
(514, 25, 'Mechanical', 15),
(515, 44, 'BPharm', 219),
(516, 9, 'Civil ', 221),
(517, 1, 'Applied Mechanics', 221),
(518, 11, 'Computer', 221),
(519, 45, 'Electronics', 221),
(520, 14, 'Electrical', 221),
(521, 25, 'Mechanical', 221),
(522, 32, 'Production', 221),
(523, 13, 'Electronics & Communication', 221),
(524, 21, 'Information & Technology', 221),
(525, 25, 'Maths', 221);

-- --------------------------------------------------------

--
-- Table structure for table `org_mst`
--

CREATE TABLE `org_mst` (
  `org_id` int(11) NOT NULL,
  `org_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_mst`
--

INSERT INTO `org_mst` (`org_id`, `org_name`) VALUES
(1, 'Institute of National Repute'),
(2, 'State Level University'),
(3, 'DTE Gujarat Institute'),
(4, 'State Level Institute'),
(5, 'Video Conference'),
(6, 'NITTTR'),
(7, 'MOOC'),
(8, 'Other'),
(9, 'ISTE/AICTE'),
(10, 'UGC/MHRD');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `REGID` int(11) NOT NULL,
  `College_Name` varchar(255) DEFAULT NULL,
  `College_Id` varchar(255) DEFAULT NULL,
  `Salutation` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `Person_Name` varchar(255) DEFAULT NULL,
  `Last_Name` varchar(255) DEFAULT NULL,
  `Gender` varchar(255) DEFAULT NULL,
  `DesignationName` varchar(255) DEFAULT NULL,
  `Designation_Id` int(11) DEFAULT NULL,
  `DepartmentName` varchar(255) DEFAULT NULL,
  `Department_Id` int(11) DEFAULT NULL,
  `JoinDate` varchar(255) DEFAULT NULL,
  `StaffType` varchar(255) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `ResidentAddress` varchar(255) DEFAULT NULL,
  `Qualification` varchar(255) DEFAULT NULL,
  `Branch` varchar(255) DEFAULT NULL,
  `DOB` varchar(255) DEFAULT NULL,
  `GRADEPAY` varchar(255) DEFAULT NULL,
  `Institutetype` varchar(255) DEFAULT NULL,
  `Institutetype_Id` varchar(255) DEFAULT NULL,
  `Role_Id` int(11) DEFAULT NULL,
  `InsertedBy` int(11) DEFAULT NULL,
  `InsertedOn` varchar(255) DEFAULT NULL,
  `DeletedBy` int(11) DEFAULT NULL,
  `DeletedOn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`REGID`, `College_Name`, `College_Id`, `Salutation`, `Surname`, `Person_Name`, `Last_Name`, `Gender`, `DesignationName`, `Designation_Id`, `DepartmentName`, `Department_Id`, `JoinDate`, `StaffType`, `Mobile`, `Email`, `ResidentAddress`, `Qualification`, `Branch`, `DOB`, `GRADEPAY`, `Institutetype`, `Institutetype_Id`, `Role_Id`, `InsertedBy`, `InsertedOn`, `DeletedBy`, `DeletedOn`) VALUES
(1, 'GP, Ahmedabad', '25', 'Ms.', 'Patel', 'Jyoti', 'M', 'Female', 'FACULTY', 6, 'IC Engg.', 22, '26-May-95', 'Regular', '9375967648', 'mohitmurlidhar@gmail.com', '', 'ME/M.TECH', '', '17-Dec-67', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2, 'GP, Ahmedabad', '25', 'Mr.', 'Prajapati', 'Bhavesh', 'S', 'Male', 'FACULTY', 6, 'IC Engg.', 22, '26-May-95', 'Regular', '9375967648', 'parinpatel@gmail.com', '', 'ME/M.TECH', '', '17-Dec-67', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3, 'GP, Ahmedabad', '25', 'Mr.', 'Brahmbhatt', 'Girish', 'D', 'Male', 'FACULTY', 6, 'IC Engg.', 22, '26-May-95', 'Regular', '9375967648', 'vishal@gmail.com', '', 'ME/M.TECH', '', '17-Dec-67', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(4, 'GP, Ahmedabad', '25', 'Ms.', 'Ramanujam', 'Purvi', 'R', 'Female', 'HOD', 7, 'IC Engg.', 22, '26-May-95', 'Regular', '9375967648', 'vatsal@gmail.com', '', 'ME/M.TECH', '', '17-Dec-67', '', 'MCA', '2', 3, 0, '0', 0, '0'),
(117, 'GP, Ahmedabad', '25', 'Mr.', 'Patel', 'Dixit', 'CHANDRASHANKAR', 'Male', 'Lecturer', 6, 'Applied Mechanics', 1, '26-May-95', 'Regular', '9375967648', 'kiritcbhatt@gmail.com', '', 'ME/M.TECH', '', '17-Dec-67', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(118, 'GP, Ahmedabad', '25', 'Mr.', 'Desai', 'PAYAL', 'VINAYCHANDRA', 'Female', 'Lecturer', 6, 'Applied Mechanics', 1, '28-Feb-86', 'Regular', '9998561937', 'sunildoshi305@gmail.com', '', 'ME/M.TECH', '', '30-May-64', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(119, 'GP, Ahmedabad', '25', 'Mr.', 'Choksi', 'Tejash', 'PURUSHOTTAMBHAI', 'Male', 'Lecturer', 6, 'Applied Mechanics', 1, '2005-03-23', 'Regular', '9408780317', 'hiteshkanani@rediffmail.com', 'F-904, Tirupati Akruti Greenz, Opp. Swarnim Square, Nirma Vidhyavihar School Road, Gota, Ahmedabad-382481', 'ME/MTech', '1', '1978-03-20', '8000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(120, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'Bharat', 'RAMANLAL', 'Male', 'Lecturer', 6, 'Applied Mechanics', 1, '20-Aug-91', 'Regular', '9427144318', 'brpatel1964@gmail.com', '', 'ME/M.TECH', '', '01-Jun-64', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(121, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'DIVYA', 'MOHANBHAI', 'Female', 'Lecturer', 6, 'Applied Mechanics', 1, '27-May-11', 'Regular', '9974116123', 'divya0120@gmail.com', '', 'ME/M.TECH', '', '22-Jun-84', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(122, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'NIKESHKUMAR', 'NARSINHBHAI', 'Male', 'Lecturer', 6, 'Applied Mechanics', 1, '20-Sep-94', 'Regular', '9426060710', 'nikeshpatel 65 @yahoo.com', '', 'BE/B.TECH', '', '24-Oct-65', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(123, 'GP, Ahmedabad', '25', 'Mr.', 'PATHAN', 'MOHAMMEDJAMIL', 'ABDULSALIM', 'Male', 'Lecturer', 6, 'Applied Mechanics', 1, '1998-04-07', 'Regular', '9979772299', 'map.2654@gmail.com', '', 'ME/MTech', '1', '1976-11-11', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(245, 'GP, Ahmedabad', '25', 'Mr.', 'CHANGELA', 'ARYA', 'BHARATBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '07-Nov-17', 'Regular', '9879571407', 'arya.changela@gmail.com', '', 'ME/M.TECH', '', '23-Oct-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(246, 'GP, Ahmedabad', '25', 'Mr.', 'CHAROLA', 'HARDIK', 'BHUPENDRARAY', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '27-Oct-17', 'Regular', '7567963778', 'charola004@gmail.com', '', 'ME/M.TECH', '', '31-Oct-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(247, 'GP, Ahmedabad', '25', 'Mr.', 'DHANAK ', ' DILIPKUMAR', ' VAKTAJI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '25-Sep-08', 'Regular', '8000130135', 'dhanak-dilip2891@yahoo.co.in', '', 'ME/M.TECH', '', '28-Sep-85', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(248, 'GP, Ahmedabad', '25', 'Ms.', 'HADIYAL ', 'VARSHA', 'KALYANJIBHAI', 'Female', 'Lecturer', 6, 'Automobile Engg.', 3, '01-Dec-17', 'Regular', '8000073893', 'hadiyalvarsha@yahoo.com', '', 'BE/B.TECH', '', '16-Sep-84', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(249, 'GP, Ahmedabad', '25', 'Mr.', 'JANSARI', 'SANJAY', 'RASIKBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '11-Sep-15', 'Regular', '9510995998', 'sanjayjansari8614@gmail.com', '', 'BE/B.TECH', '', '14-Oct-86', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(250, 'GP, Ahmedabad', '25', 'Mr.', 'KANDIYA', ' TEJASKUMAR', ' KANJIBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '2011-04-08', 'Regular', '9428463021', 'tkkandiya@gmail.com', '1-SHRINATH RAJPATHNAGAR BOPAL AHMEDABAD 380058', 'ME/MTech', '25', '1984-03-21', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(251, 'GP, Ahmedabad', '25', 'Mr.', 'KAPADIA', 'GARVISH', 'ASHVINKUMAR', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '08-Apr-11', 'Regular', '9033548412', 'garvish.k@rediffmail.com', '', 'ME/M.TECH', '', '18-Feb-77', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(252, 'GP, Ahmedabad', '25', 'Mr.', 'NANAVATI ', ' ASHESHKUMAR ', 'KESHAVLAL', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '02-Mar-95', 'Regular', '9426164409', 'aknanavati@gmail.com', '', 'ME/M.TECH', '', '19-Oct-67', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(253, 'GP, Ahmedabad', '25', 'Mr.', 'PARIKH', 'ALKESH', 'GIRISHBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '10-May-91', 'Regular', '9228198308', 'alkesh138@gmail.com', '', 'BE/B.TECH', '', '13-Aug-68', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(254, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'KEVAL', 'JYOTINDRAKUMAR', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '21-Sep-15', 'Regular', '9925622770', 'keval.j.patel@gmail.com', '', 'ME/M.TECH', '', '14-Jul-88', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(255, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'VISHAL', 'BHARATBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '2010-11-02', 'Regular', '9428218788', 'vixhal@gmail.com', 'B 2 Madhav Vihar Bungalow, Tapovan circle, Chand kheda, Ahmedabad', 'ME/MTech', '25', '1982-01-08', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(256, 'GP, Ahmedabad', '25', 'Mr.', 'PRAJAPATI', 'BHARATKUMAR', 'BHIKHABHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '28-Apr-11', 'Regular', '9825109959', 'write2bprajapati@gmail.com', '', 'ME/M.TECH', '', '22-Jun-85', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(257, 'GP, Ahmedabad', '25', 'Mr.', 'RAVAL', 'NILAY', 'ATULKUMAR', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '2015-09-21', 'Regular', '9408508868', 'nilay.auto.gpa@gmail.com', '3/NANDANVAN SOCIETY, SARVAJANIK HOSPITAL ROAD, MODASA - 383315, DIST:- ARAVALLI', 'ME/MTech', '3', '1990-11-18', '5400', 'DIPLOMA', '2', 0, 0, '0', 0, '0'),
(258, 'GP, Ahmedabad', '25', 'Ms.', 'SONI', 'JAHNAVI', 'JAYESHBHAI', 'Female', 'Lecturer', 6, 'Automobile Engg.', 3, '15-Sep-15', 'Regular', '9409167891', 'sonijahnavi@ymail.com', '', 'BE/B.TECH', '', '13-Aug-92', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(259, 'GP, Ahmedabad', '25', 'Mr.', 'THAKKAR', 'AKSHAY', 'DILIPBHAI', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '03-Nov-17', 'Regular', '9033538553', 'akshaythakkar.1993@gmail.com', '', 'ME/M.TECH', '', '06-Apr-92', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(260, 'GP, Ahmedabad', '25', 'Mr.', 'VASAVA', 'JAGDISHKUMAR', 'BHARATKUMR', 'Male', 'Lecturer', 6, 'Automobile Engg.', 3, '18-Jun-16', 'Regular', '9898661912', 'vasavajagdish_91@yahoo.com', '', 'ME/M.TECH', '', '18-Jul-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(300, 'GP, Ahmedabad', '25', 'Mr.', 'LIMBAD', 'NAVDEEPSINH', 'VIRSANGBHAI', 'Male', 'Lecturer', 6, 'Bio-Medical', 4, '25-Feb-16', 'Regular', '9925408474', 'limbad.navdeep10@gmail.com', '', 'ME/M.TECH', '', '05-Jul-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(301, 'GP, Ahmedabad', '25', 'Ms.', 'MALKAN', 'SHRUSHTI', 'SUNILKUMAR', 'Female', 'Lecturer', 6, 'Bio-Medical', 4, '23-Feb-16', 'Regular', '9016614417', 'shrushtimalkan@gmail.com', '', 'BE/B.TECH', '', '10-Oct-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(302, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'CHINTAN', 'SHAILESHBHAI', 'Male', 'Lecturer', 6, 'Bio-Medical', 4, '05-Mar-13', 'Contractual', '9033277464', 'shahchintan35@gmail.com', '', 'BE/B.TECH', '', '13-Apr-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(303, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'JAINAMKUMAR', 'SASHIKANTBHAI', 'Male', 'Lecturer', 6, 'Bio-Medical', 4, '18-Mar-16', 'Regular', '9428291742', 'shahjainamic@yahoo.com', '', 'ME/M.TECH', '', '30-Apr-86', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(304, 'GP, Ahmedabad', '25', 'Ms.', 'SOLANKI', 'MOHINI', 'PANKAJKUMAR', 'Female', 'Lecturer', 6, 'Bio-Medical', 4, '04-Mar-13', 'Contractual', '9429204248', 'solankimohini2802@gmail.com', '', 'BE/B.TECH', '', '28-Feb-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(485, 'GP, Ahmedabad', '25', 'Ms.', 'MAMPILLY', 'REHANA', 'BAIJU', 'Female', 'Lecturer', 6, 'Chemistry', 8, '2011-02-01', 'Regular', '8758267072', 'rehanabaijum@gmail.com', '91,JYOTIKALASH SOC., NR.ISRO, S.M.ROAD, SATELLITE, AHMEDABAD 380015', 'Msc', '8', '1972-05-31', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(486, 'GP, Ahmedabad', '25', 'Mr.', 'Rathvi', 'Anil', 'Kanubhai', 'Male', 'Lecturer', 6, 'Chemistry', 8, '27-Oct-17', 'Regular', '9638400023', '', '', 'Doctorate', '', '16-Nov-86', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(678, 'GP, Ahmedabad', '25', 'Mr.', 'AHIR', 'HITESH', 'HADABHAI', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '15-Mar-10', 'Regular', '8141180109', 'hitesh.ahir@ymail.com', '', 'ME/M.TECH', '', '08-Aug-80', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(679, 'GP, Ahmedabad', '25', 'Ms.', 'DESAI', 'MANISHA', 'MINESH', 'Female', 'Lecturer', 6, 'Civil Engg.', 9, '07-Mar-92', 'Regular', '9427320990', 'manimin1964@yahoo.co.in', '', 'ME/M.TECH', '', '02-Oct-64', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(680, 'GP, Ahmedabad', '25', 'Ms.', 'JAIN', 'SONAL', 'MULCHANDBHAI', 'Female', 'Lecturer', 6, 'Civil Engg.', 9, '14-Nov-90', 'Regular', '7874679680', 'sonaldipeshshah@gmail.com', '', 'ME/M.TECH', '', '30-Jun-68', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(681, 'GP, Ahmedabad', '25', 'Ms.', 'JHA', 'ALPANA', 'TARUNKUMAR', 'Female', 'Lecturer', 6, 'Civil Engg.', 9, '12-Mar-01', 'Regular', '9427314963', 'alpanajha1@yahoo.co.in', '', 'ME/M.TECH', '', '21-Apr-67', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(682, 'GP, Ahmedabad', '25', 'Mr.', 'JHA ', ' NILESHKUMAR', ' SHRINARAYAN', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '03-Jul-98', 'Regular', '9227588226', 'nileshjha75@gmail.com', '', 'Doctorate', '', '08-Dec-75', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(683, 'GP, Ahmedabad', '25', 'Mr.', 'MAKWANA', 'BHARGAV', 'MAHESHKUMAR', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '10-Jun-11', 'Regular', '9998348496', 'bhargav_makwana@yahoo.co.in', '', 'ME/M.TECH', '', '29-Apr-86', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(684, 'GP, Ahmedabad', '25', 'Mr.', 'MEHTA', 'PRANAV', 'PANKAJKUMAR', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '17-Jun-16', 'Regular', '9904779952', 'pranavhy@gmail.com', '', 'ME/M.TECH', '', '20-Jan-85', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(685, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'ASHUTOSH', 'KANTIBHAI', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '29-Oct-91', 'Regular', '9825009719', 'ashutech.asp@gmail.com', '', 'Doctorate', '', '23-Dec-66', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(686, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'CHIRAG', 'BHAVANBHAI', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '29-Dec-15', 'Regular', '7567647301', 'patel.chig@gmail.com', '', 'ME/M.TECH', '', '05-Aug-82', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(687, 'GP, Ahmedabad', '25', 'Mr.', 'RAJGOR ', 'NARENDRAKUMAR', ' NATVARLAL', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '21-Sep-94', 'Regular', '9909253651', 'narendrarajror@yahoo.com', '', 'ME/M.TECH', '', '02-Jun-66', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(688, 'GP, Ahmedabad', '25', 'Mr.', 'ROHIT', 'GOPALBHAI', 'RANCHHODBHAI', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '19-Sep-94', 'Regular', '9979869860', 'grrohit2002@gmail.com', '', 'ME/M.TECH', '', '01-Jun-65', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(689, 'GP, Ahmedabad', '25', 'Mr.', 'VARMORA ', 'KETANKUMAR ', 'CHHAGANLAL', 'Male', 'Lecturer', 6, 'Civil Engg.', 9, '2005-03-16', 'Regular', '9725335599', 'kcvarmora@gmail.com', 'M-m Block 39/307, Shastrinagar Government Colony, Naranpura, Ahmedabad', 'ME/MTech', '9', '1971-06-16', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1052, 'GP, Ahmedabad', '25', 'Mr.', 'ACHARYA', 'JIGER', 'PRAKASHCHANDRA', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2002-01-09', 'Regular', '9429462026', 'jigeracharya@gmail.com', 'b-303 nilkanth 2, opp. netra banglows,navrangpura ahmedabad', 'ME/MTech', '11', '1979-05-17', '8000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1053, 'GP, Ahmedabad', '25', 'Ms.', 'BHADARKA', 'MITABEN', 'MOHITBHAI', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '12-Jul-11', 'Regular', '9586857997', 'mjbheda@gmail.com', '', 'BE/B.TECH', '', '14-Nov-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1054, 'GP, Ahmedabad', '25', 'Ms.', 'BHARVAD', 'GAYATRI', 'SIBHABHAI', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '30-Sep-08', 'Adhoc', '9099506699', 'bharvad.gayatri@gmail.com', '', 'BE/B.TECH', '', '20-May-87', '', 'DIPLOMA', '2', 7, 0, '0', 1459, '07/06/2018 23:33:47'),
(1055, 'GP, Ahmedabad', '25', 'Mr.', 'BHAVSAR', 'CHINTANKUMAR', 'KIRITKUMAR ', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-11-16', 'Regular', '9998825984', 'ck.bhavsar@gmail.com', 'C-504, Shukan Villas, Sargasan Cross roads, Gandhinagar', 'ME/MTech', '11', '1983-02-16', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1056, 'GP, Ahmedabad', '25', 'Mr.', 'DESAI', 'MANISH', 'JINANLAL', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '11-Jul-95', 'Regular', '9427030047', 'mjdesai34@yahoo.co.in', '', 'ME/M.TECH', '', '08-Sep-72', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1057, 'GP, Ahmedabad', '25', 'Ms.', 'GOSWAMI', 'KHYATI', 'PRADEEP', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '11-Nov-16', 'Regular', '9978781018', 'khyati.goswami@gmail.com', '', 'BE/B.TECH', '', '01-Jul-88', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1058, 'GP, Ahmedabad', '25', 'Mr.', 'JAMBUKIA', 'RAJESH', 'TRIBHOVAN', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-11-02', 'Regular', '9429363089', 'jambukia.rajesh@gmail.com', '', 'BE/BTech', '11', '1987-09-10', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1059, 'GP, Ahmedabad', '25', 'Ms.', 'KANZARIYA', 'DHARABEN', 'MADHUBHAI', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '29-Oct-16', 'Regular', '9428107509', 'kanzariya.dharagp@gmail.com', '', 'BE/B.TECH', '', '25-Feb-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1060, 'GP, Ahmedabad', '25', 'Mrs', 'KATPARA', 'HEMLATA', ' VIJAYKUMAR', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '2010-01-19', 'Regular', '9428736195', 'hkatpara@gmail.com', 'C 101 MANOKAMNA TENEMENTS NR. MUKHI NI WADI , ISANPUR-VATVA ROAD ISANPUR AHMEDABAD', 'ME/M.TECH', '11', '1978-04-14', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1061, 'GP, Ahmedabad', '25', 'Ms.', 'LEUVA', 'CHARULATA', 'BHIKHABHAI', 'Female', 'Lecturer', 6, 'Computer engg.', 11, '07-Oct-08', 'ADHOC', '9662045575', 'charu_cengg@yahoo.com', '', 'BE/B.TECH', '', '07-Sep-82', '', 'DIPLOMA', '2', 7, 0, '0', 1459, '08/06/2018 23:02:06'),
(1062, '18', '25', 'Mr.', 'MAKWANA', 'CHINTAN', 'HIMATLAL', 'Male', 'Assistant Professor', 6, '21', 11, '2016-10-28', 'Regular', '9033166626', 'chintan.h.makwana@gmail.com', '', 'ME/MTech', '11', '1991-08-01', '6000', 'Degree', '2', 0, 0, '0', 0, '0'),
(1063, 'GP, Ahmedabad', '25', 'Mr.', 'MEHTA', 'MIHIR', 'DINESHBHAI', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-11-10', 'Regular', '8905735795', 'mihir240491@gmail.com', '', 'ME/MTech', '11', '1991-04-24', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1064, 'GP, Ahmedabad', '25', 'Mr.', 'PARMAR', 'HARSHAD', 'RAMJIBHAI', 'Male', 'HOD', 3, 'Computer Engg.', 11, '02-Jun-95', 'Regular', '9925832110', 'gpc-sab@gujarat.gov.in', '', 'ME/M.TECH', '', '11-Mar-68', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(1065, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'KINJALKUMARI', 'KANTILAL', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '09-Jun-11', 'Regular', '9879133163', 'ce_kinjal01@yahoo.co.in', '', 'ME/M.TECH', '', '03-Sep-80', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1066, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'UMANG', 'DEVENDRAKUMAR', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-10-28', 'Regular', '9427686364', 'umang.shah111gp@gmail.com', '', 'ME/MTech', '11', '1988-01-29', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1067, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'VAIDEHI ', 'DIPAKKUMAR ', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '08-Oct-12', 'Contractual', '9173098129', 'vaidehishah16@gmail.com', '', 'BE/B.TECH', '', '08-Jul-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1068, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH ', 'REKHA', ' MANISH', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '17-Feb-97', 'Regular', '9638532678', 'rekhamanishshah@gmail.com', '', 'ME/M.TECH', '', '13-Sep-72', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1069, 'GP, Ahmedabad', '25', 'Ms.', 'SHARMA', 'BHOOMIKA', 'KRISHNANANDAN', 'Female', 'Lecturer', 6, 'Computer Engg.', 11, '2008-08-06', 'Regular', '9879074125', 'sharma.bhoomika02@gmail.com', '', 'ME/M.TECH', '11', '1981-11-02', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1070, 'GP, Ahmedabad', '25', 'Mr.', 'SONI', 'VINAYKUMAR', 'NARENDRAKUMAR', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-11-07', 'Regular', '9016665573', 'vinaysonilive@gmail.com', '', 'ME/MTech', '11', '1989-08-12', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1071, 'GP, Ahmedabad', '25', 'Mr.', 'TANK', 'YAGNIK', 'NARSINHBHAI', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-11-03', 'Regular', '9429515076', 'tankyagnik19@gmail.com', '', 'BE/B.TECH', '11', '1991-01-19', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1072, 'GP, Ahmedabad', '25', 'Mr.', 'THAKER', 'ALPESHKUMAR', 'RAJNIKANTBHAI', 'Male', 'Lecturer', 6, 'Computer Engg.', 11, '2016-10-28', 'Regular', '9879709675', 'alpeshrthaker@gmail.com', '', 'BE/BTech', '11', '1985-01-28', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1446, 'GP, Ahmedabad', '25', 'Mr.', 'BUCH', 'UJJVAL', 'VAMANRAY', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '24-Feb-95', 'Regular', '9825346922', 'uvbuch@yahoo.co.in', '', 'ME/M.TECH', '', '23-Mar-69', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1447, 'GP, Ahmedabad', '25', 'Mr.', 'CHANDEGARA', 'AJAY', 'RAMJIBHAI', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '1992-11-19', 'Regular', '9898032871', 'ajay_chandegara@yahoo.com', 'A2, ved appt. b/h. Star bazar, Satellite Road , Ahmedabad', 'ME/MTech', '14', '1969-04-18', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1448, 'GP, Ahmedabad', '25', 'Mr.', 'CHANPURA', 'TEJAS', 'PRAVINCHANDRA', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '10-Dec-93', 'Regular', '9824280515', 'tchanpura@yahoo.com', '', 'ME/M.TECH', '', '06-Dec-69', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1449, 'GP, Ahmedabad', '25', 'Ms.', 'CHAUHAN', 'JANHVI', 'ARAVINDBHAI', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '2011-04-21', 'Regular', '9537648557', 'jhanvc@gmail.com', '', 'ME/MTech', '13', '1987-08-08', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1450, 'GP, Ahmedabad', '25', 'Mr.', 'DAVE', 'MIHIR', 'SURESHBHAI', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '1994-03-05', 'Regular', '9714766944', 'mihir69_dave@yahoo.com', 'c-302,shukan-4,ankur,naranpura,ahd-13', 'ME/MTech', '45', '1969-07-01', '9000', 'DIPLOMA', '2', 0, 0, '0', 0, '0'),
(1451, 'Government Polytechnic, Ahmedabad', '25', 'Mr.', 'DHUMALE', 'JITENKUMAR', 'ARUNBHAI', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '2016-10-28', 'Regular', '9157057686', 'jitendhumale.ec@gmail.com', 'C-302 SHREE NAND CITY-4, NEAR DOON SCHOOL, NEW MANINAGAR, RAMOL, AHMEDABAD', 'BE/BTech', '13', '1992-04-25', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1452, 'GP, Ahmedabad', '25', 'Mr.', 'KATARIA', 'SATISHKUMAR', 'MANSUKHLAL', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '2016-10-28', 'Regular', '9998991632', 'smkataria.ec@gmail.com', 'B-9/43, VAIKUTH PARK, NR CADILA BRIDGE, GHODASAR, AHMEDABAD 380050', 'ME/MTech', '13', '1986-02-10', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1453, 'GP, Ahmedabad', '25', 'Mr.', 'MEHTA', 'RUTUL', 'DINESHCHANDRA', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '11-May-05', 'Regular', '9408272157', 'Rutuldmehta@gmail.com', '', 'ME/M.TECH', '', '11-May-79', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1454, 'GP, Ahmedabad', '25', 'Mr.', 'MERCHANT', ' NILESH', ' RAGHUVIR', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '24-Nov-93', 'Regular', '9825465702', 'nrmerchant2002@yahoo.co.in', '', 'ME/M.TECH', '', '09-Mar-69', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1455, 'GP, Ahmedabad', '25', 'Mr.', 'MODI', 'DHAVAL', 'MAHESHKUMAR', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '09-May-11', 'Regular', '9429613765', 'dhavalmodi045@yahoo.com', '', 'ME/M.TECH', '', '13-Mar-88', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1456, 'GP, Ahmedabad', '25', 'Mr.', 'PANCHAL', 'HITESH', 'DAHYABHAI', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '19-Jan-02', 'Regular', '9998598273', 'hdpanchal.lec@gmail.com', '', 'ME/M.TECH', '', '08-Dec-79', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1457, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'APURVA', 'SURESHBHAI', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '2016-11-21', 'Regular', '9426027975', 'apurvsept85@gmail.com', '', 'BE/BTech', '13', '1985-08-05', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1458, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'ASHA', 'PINALKUMAR', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '19-Apr-05', 'Regular', '9925614157', 'asha_ppatel@yahoo.com', '', 'ME/M.TECH', '', '20-May-80', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1459, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'PANKAJ', 'MANUBHAI', 'Male', 'HOD', 3, 'EC Engg.', 13, '25-Mar-97', 'Regular', '9825022025', 'pankaj_patel@rediffmail.com', '', 'ME/M.TECH', '', '02-Feb-64', '', 'DIPLOMA', '2', 2, 0, '0', 0, '0'),
(1460, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'PURVIBEN', 'ARVINDBHAI', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '16-Sep-15', 'Regular', '7359502748', 'purvipatel369@gmail.com', '', 'ME/M.TECH', '', '03-Jun-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1461, 'GP, Ahmedabad', '25', 'Mrs', 'PRAJAPATI', 'MONALI', 'RAKESHKUMAR', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '2005-04-19', 'Regular', '7490052256', 'monalimandli79@gmail.com', '17,Tulshi Bunglows, Gota Ahmedabad', 'ME/M.TECH', '13', '1979-05-05', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1462, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'KEROLIN', 'KUMUDBHAI', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '1998-04-23', 'Regular', '9427026559', 'keroshah@gmail.com', 'C 901 sagun castle, B/h star *India Bazar satellite', 'ME/MTech', '13', '1973-07-11', '7000', 'DIPLOMA', '2', 0, 0, '0', 0, '0'),
(1463, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'MANSI', 'PRAVINKUMAR', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '2011-05-02', 'Regular', '9429281888', 'mansi_ddit07@yahoo.co.in', '27 ambarish soc , radhaswami satsang road , ranip ahmedabad', 'ME/MTech', '13', '1985-12-04', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1464, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH ', 'RAJIV', ' BAKULCHANDRA', 'Male', 'Lecturer', 6, 'EC Engg.', 13, '18-Dec-92', 'Regular', '9824801912', 'rbs_gpa@yahoo.com', '', 'ME/M.TECH', '', '30-Jan-70', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1465, 'GP, Ahmedabad', '25', 'Ms.', 'VORA', 'LOPA', 'JILPESH', 'Female', 'Lecturer', 6, 'EC Engg.', 13, '10-Apr-97', 'Regular', '9408803778', 'lopa_67@yahoo.co.in', '', 'ME/M.TECH', '', '06-Jul-72', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1834, 'GP, Ahmedabad', '25', 'Mr.', 'BAQUI', 'MATINAHMED', ' SHAKURBHAI', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '2001-07-12', 'Regular', '9426543242', 'matin_baqui@hotmail.com', '', 'ME/M.TECH', '14', '1974-04-10', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1835, 'GP, Ahmedabad', '25', 'Mr.', 'CHOKSI', 'HEMISH', 'ROHITBHAI', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '17-Jul-98', 'Regular', '9825271236', 'hemishchoksi@yahoo.com', '', 'ME/M.TECH', '', '14-Feb-76', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1836, 'GP, Ahmedabad', '25', 'Mr.', 'KAPADIYA', 'HITESH', 'BALDEVBHAI', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '2001-01-11', 'Regular', '9427600807', 'hitubk@gmail.com', '', 'ME/MTech', '14', '1978-01-24', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1837, 'GP, Ahmedabad', '25', 'Ms.', 'PANDYA', 'BEENA', 'PIYUSH', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '27-Oct-97', 'Regular', '9924603375', 'beena.gohil@gmail.com', '', 'ME/M.TECH', '', '11-Mar-73', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1838, 'GP, Ahmedabad', '25', 'Mr', 'PARMAR ', 'DIPAKKUMAR', 'GIRISHBHAI', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '2005-03-09', 'Regular', '9725058362', 'dgparmar2003@gmail.com', '', 'ME/MTech', '14', '1981-05-08', '8000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1839, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'FALGUNI', 'MANUBHAI', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '07-Jul-17', 'Regular', '9879161235', 'patelfalu12@gmail.com', '', 'ME/M.TECH', '', '02-Sep-79', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1840, 'GP, Ahmedabad', '25', 'Mr', 'PATEL', 'PINKALKUMAR', 'JASHVANTBHAI', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '2016-10-28', 'Regular', '9426507520', 'pjpatel.ee@gmail.com', '', 'ME/MTech', '14', '1981-08-31', '6000', 'DIPLOMA', '2', 0, 0, '0', 0, '0'),
(1841, 'GP, Ahmedabad', '25', 'Ms.', 'RANA', 'ANJALIBA', 'DIGVIJAYSINH', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '10-Aug-17', 'Regular', '7878246658', 'anjali_22590@yahoo.com', '', 'ME/M.TECH', '', '22-May-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1842, 'GP, Ahmedabad', '25', 'Ms.', 'RATHOD', 'ANJALI', 'MANUBHAI', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '13-Aug-08', 'Adhoc', '7600050199', 'anjali25_rathod@yahoo.co.in', '', 'BE/B.TECH', '', '19-Jul-81', '', 'DIPLOMA', '2', 7, 0, '0', 1459, '25/06/2018 22:21:49'),
(1843, 'GP, Ahmedabad', '25', 'Ms.', 'RAVAL', 'DIPIKA', 'KANUBHAI', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '16-Apr-10', 'Regular', '9099011941', 'dipikacusp@gmail.com', '', 'ME/M.TECH', '', '12-Nov-78', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(1844, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'ALKESH', 'DHIRAJLAL', 'Male', 'Lecturer', 6, 'Electrical Engg.', 14, '1992-04-23', 'Regular', '9033220605', 'shah_alkesh@gtu.edu.in', '56-KAMLAPARK SOCIETY, NEAR CADILA LABORATORY, GHODASAR, AHMEDABAD - 380 050', 'ME/MTech', '14', '1969-10-26', '9000', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(1845, 'GP, Ahmedabad', '25', 'Ms.', 'SHARMA', 'PRITI', 'YATISHCHANDRA', 'Female', 'Lecturer', 6, 'Electrical Engg.', 14, '26-Apr-10', 'Regular', '9099051708', 'pshar@gmail.com', '', 'BE/B.TECH', '', '22-Jun-82', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2206, 'GP, Ahmedabad', '25', 'Ms.', 'JUNEJA', 'ALMASARA', 'MUSTUFABHAI', 'Female', 'Lecturer', 6, 'English', 15, '29-Aug-98', 'Regular', '9099063099', 'almasjuneja@yahoo.co.in', '', 'M.phil', '', '15-Feb-74', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2207, 'GP, Ahmedabad', '25', 'Mr.', 'KHAN', 'ABDUALKAYUM', 'RAMZANIKHAN', 'Male', 'Lecturer', 6, 'English', 15, '29-Jul-97', 'Adhoc', '9426271114', 'arkhan32@gmail.com', '', 'M.A.English', '', '03-Sep-65', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2208, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'DURLABHBHAI', 'MAGANBHAI', 'Male', 'Lecturer', 6, 'English', 15, '23-Dec-93', 'Regular', '8733996896', 'durlabh1964@gmail.com', '', 'ME/M.TECH', '', '05-May-64', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2319, 'GMCA,Maninagar', '14', 'Mr.', 'Prajapati', 'Bhavesh', 'Babubhai', 'Male', 'AP (Cl-2)', 5, 'Info. Tech.', 21, '18-Feb-05', 'Regular', '', '', '', 'ME/M.TECH', '', '15-Jun-75', '', 'DEGREE', '1', 7, 0, '0', 0, '0'),
(2333, 'GMCA,Maninagar', '14', 'Ms.', 'Ramanuj', 'Purvi', 'Nirajbhai', 'Male', 'AP (Cl-2)', 5, 'IT Engg.', 21, '2005-02-22', 'Regular', '9427007640', 'purviramanuj@yahoo.com', '', 'ME/MTech', '21', '1979-07-28', '7000', 'Degree', '1', 7, 0, '0', 0, '0'),
(2372, 'GP, Ahmedabad', '25', 'Mr.', 'CHAUHAN', 'RUSHAL', 'GAUTAMBHAI', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '2016-12-22', 'Regular', '9429355111', 'chauhanrushal@gmail.com', '22 Kulin Tenaments,B/H G.B.Shah College,Vasna,Ahmedabad-7', 'ME/MTech', '21', '1989-05-20', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2373, 'GP, Ahmedabad', '25', 'Mr.', 'FATAK', 'NANDU', 'ASHOKBHAI', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '18-Mar-05', 'Regular', '9033870918', 'nandufatak@gmail.com', '', 'ME/M.TECH', '', '23-Nov-78', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(2374, 'GP, Ahmedabad', '25', 'Ms.', 'GARG ', ' SHEENAM ', 'PARVEEN', 'Female', 'Lecturer', 6, 'Info. Tech.', 21, '10-May-11', 'Regular', '9904511535', 'sheenam.it@gmail.com', '', 'ME/M.TECH', '', '13-Jul-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2375, 'GP, Ahmedabad', '25', 'Ms.', 'GOHEL', 'MEETA', 'BHARATKUMAR', 'Female', 'Lecturer', 6, 'Info. Tech.', 21, '04-Feb-14', 'Contractual', '9428950505', 'meeta.gohel19@gmail.com', '', 'BE/B.TECH', '', '05-Jul-92', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2376, 'GP, Ahmedabad', '25', 'Ms.', 'MISHRA', 'MOHINI ', 'AWADHESHKUMAR ', 'Female', 'Lecturer', 6, 'Info. Tech.', 21, '05-Oct-12', 'Contractual', '7874324638', 'mishramohini.2014@gmail.com', '', 'BE/B.TECH', '', '12-Sep-89', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2377, 'GP, Ahmedabad', '25', 'Mr.', 'PAREKH', 'MAYANK', 'VASUDEV', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '23-Dec-16', 'Regular', '9016319686', 'mayank.parekh50@gmail.com', '', 'ME/M.TECH', '', '10-Sep-88', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2378, 'GP, Ahmedabad', '25', 'Mr.', 'PRAJAPATI', 'HARDIK', 'JAYENDRAKUMAR', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '16-May-11', 'Regular', '9904975087', 'hardikjp2707@yahoo.com', '', 'BE/B.TECH', '', '27-Jul-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2379, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'NEHAL ', 'KIRITKUMAR ', 'Female', 'Lecturer', 6, 'Info. Tech.', 21, '05-Oct-12', 'Contractual', '9429901647', 'shahnehal129@gmail.com', '', 'BE/B.TECH', '', '29-May-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2380, 'GP, Ahmedabad', '25', 'Ms.', 'THAKUR', 'PRITI ', 'BHAVESHBHAI ', 'Female', 'Lecturer', 6, 'Info. Tech.', 21, '06-Oct-12', 'Contractual', '9558206274', 'pritithakur125@gmail.com', '', 'BE/B.TECH', '', '29-Nov-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2381, 'GP, Ahmedabad', '25', 'Mr.', 'VOHRA', 'SAIFEE', 'MOIJBHAI', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '26-Dec-16', 'Regular', '9426786555', 'saifeevohra@gmail.com', '', 'ME/M.TECH', '', '25-Apr-89', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2424, 'GMCA,Maninagar', '14', 'Mr.', 'FARUKI', 'PARVEZ', 'KESHARKHAN', 'Male', 'Lecturer', 6, 'Info. Tech.', 21, '2005-03-01', 'Regular', '9925199198', 'parvezfaruki.kg@gmail.com', '', 'Phd', '21', '1977-06-27', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2479, 'GOVERNMENT MCA COLLEGE', '25', 'Mr.', 'BHATT', 'CHETAN', 'BHASKERTAY', 'Male', 'Principal', 1, 'IC Engg.', 22, '1991-11-02', 'Regular', '9427624727', 'chetan_bhatt@yahoo.com', '17 Ravikrupa Society, Near Vishweshwar Mahadev, Satellite, Ahmedabad', 'Phd', '22', '1968-11-02', '10000', 'DEGREE', '1', 2, 0, '0', 0, '0'),
(2527, 'GP, Ahmedabad', '25', 'Ms.', 'DAVE', 'HEENA', 'JAGDISHCHANDRA', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '2015-11-03', 'Regular', '9426735838', 'hjdave23@gmail.com', 'plot no.-18,sumeru bunglows,near TOP-3 circle,Talaja road,Bhavnagar', 'BE/BTech', '22', '1972-12-23', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2528, 'GP, Ahmedabad', '25', 'Mr.', 'GANDHI', 'SHRIJI', 'VIKRAMBHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '2015-10-19', 'Regular', '9510352451', 'shrijigandhi.007@gmail.com', '', 'ME/MTech', '22', '1990-11-22', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2529, 'GP, Ahmedabad', '25', 'Ms.', 'JOSHI', 'DARSHNA', 'MANOJKUMAR', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '2015-10-15', 'Regular', '9824549854', 'darshna.joshi91@gmail.com', '59/232, GOVT \'E\' COLONY, OPP-AMBER CINEMA BAPUNAGAR, AHMEDABAD-380024', 'ME/MTech', '22', '1991-08-27', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2530, 'GP, Ahmedabad', '25', 'Mr.', 'MAHETA', 'NAIMESH', 'BHUPENDRABHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '07-Apr-00', 'Regular', '9428355370', 'naimesh1968@gmail.com', '', 'ME/M.TECH', '', '17-Dec-68', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2531, 'GP, Ahmedabad', '25', 'Ms.', 'MEHTA', 'LABDHI', 'RAJESHKUMAR', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '04-Mar-13', 'Contractual', '9033487029', 'labdhi.mehta@yahoo.in', '', 'BE/B.TECH', '', '03-Nov-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2532, 'GP, Ahmedabad', '25', 'Ms.', 'MEHTA', 'ZANKHANA', 'DILIPBHAI', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '11-Aug-08', 'Regular', '9375165738', 'zankhana_mehta@yahoo.com', '', 'BE/B.TECH', '', '08-Oct-84', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2533, 'GP, Ahmedabad', '25', 'Mrs', 'MISHRA', 'JYOTI', 'ARUNDUTT', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '2008-08-05', 'Adhoc', '9898986197', 'jyoti.astha@gmail.com', '', 'Diploma', 'IC Engg.', '1983-01-06', 'NULL', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2534, 'GP, Ahmedabad', '25', 'Mr.', 'MODI', 'PARTH', 'HASMUKHBHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '2015-10-09', 'Regular', '9662091947', 'parthmodi.ic@gmail.com', 'Jivrajpark,Ahmedabad', 'PGDC', '19', '1990-12-09', '5400', 'DIPLOMA', '2', 6, 0, 'NULL', 0, 'NULL'),
(2535, 'GP, Ahmedabad', '25', 'Mr.', 'PATANKAR', 'JAGDISH', 'TULSIBHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '19-Jul-94', 'Regular', '9825514819', 'jtpatankar1968@gmail.com', '', 'ME/M.TECH', '', '16-May-68', '', 'DIPLOMA', '2', 2, 0, '0', 0, '0'),
(2536, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL ', 'HARSHADBHAI ', ' PUNABHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '17-Aug-98', 'Regular', '9274751250', 'hartel70@radiffmail.com', '', 'ME/M.TECH', '', '18-Nov-70', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2537, 'GP, Ahmedabad', '25', 'Ms.', 'RAVAL ', ' SEJAL', ' KAMLESH', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '28-Feb-97', 'Regular', '9427952040', 'sejalr73@gmail.com', '', 'ME/M.TECH', '', '13-Mar-73', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2538, 'GP, Ahmedabad', '25', 'Mr.', 'SONI', 'URVISH', 'PRAVINKUMAR', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '14-Oct-15', 'Regular', '9428532878', 'urvish.ic@gmail.com', '', 'BE/B.TECH', '', '29-Dec-90', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2539, 'GP, Ahmedabad', '25', 'Ms.', 'SUTARIYA ', ' JOLLY', 'ASHISH', 'Female', 'Lecturer', 6, 'IC Engg.', 22, '25-Sep-97', 'Regular', '9904351257', 'jolly.sutaria@gmail.com', '', 'BE/B.TECH', '', '22-Aug-74', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2540, 'GP, Ahmedabad', '25', 'Mr.', 'VANARA', 'MANOJ', ' BATUKBHAI', 'Male', 'Lecturer', 6, 'IC Engg.', 22, '08-Mar-01', 'Regular', '9825482182', 'mbvanara@gmail.com', '', 'BE/B.TECH', '', '22-Jun-77', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2669, 'GP, Ahmedabad', '25', 'Mr.', 'CHAUDHARI', 'RAMESHCHANDRA', 'GHEMARBHAI', 'Male', 'Lecturer', 6, 'Maths', 24, '1999-07-16', 'Regular', '9998686082', 'chaudhari.rg@gmail.com', '21,SIDDHARAJ GREENS,PETHAPUR,GANDHINAGAR-382610', 'Msc', '24', '1972-02-05', '8000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2670, 'GP, Ahmedabad', '25', 'Mr.', 'CHUDASAMA', 'ASHVINSINH', 'CHANDRASINH', 'Male', 'Lecturer', 6, 'Maths', 24, '27-Sep-12', 'Regular', '9427320175', 'ashvinsinh@yahoo.co.in', '', 'M.sc', '', '17-Dec-80', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2671, 'GP, Ahmedabad', '25', 'Mr.', 'DAVE', 'JITENDRAKUMAR', 'CHANDULAL', 'Male', 'Lecturer', 6, 'Maths', 24, '1999-07-16', 'Regular', '9726963946', 'jitendradave326@yahoo.in', '24,AMIPARK nr.BHANVNA TENAMENT,GST ROAD ,RANIP', 'M.sc', '24', '1968-06-01', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(2672, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'MAMTA', 'CHAMPAKLAL', 'Female', 'Lecturer', 6, 'Maths', 24, '11-Jul-94', 'Regular', '9428361138', 'mamtasshah1992@gmail.com', '', 'M.sc', '', '17-Nov-65', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(3132, 'GP, Ahmedabad', '25', 'Mr.', 'BHATT', 'PRATHMESH', 'MAHESHBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '11-Mar-05', 'Regular', '9426525924', 'pmbhatt009@gmail.com', '', 'ME/M.TECH', '', '27-Aug-76', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3133, 'GP, Ahmedabad', '25', 'Mr.', 'CHUDASAMA', 'NATHUBHA', 'MANGALSINH', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '2006-03-20', 'Regular', '9825978190', 'nathubha@gmail.com', 'E-203, Shyam-2, Science city, SOLA, Ahmedabad -380060', 'ME/MTech', '25', '1977-04-26', '7000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3134, 'GP, Ahmedabad', '25', 'Mr.', 'HIRANI', 'MANOJKUMAR ', 'MANSUKHBHAI ', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '06-Oct-12', 'Contractual', '9601073470', 'manojhirani411991@gmail.com', '', 'BE/B.TECH', '', '04-Jan-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3135, 'GP, Ahmedabad', '25', 'Mr.', 'JAKHANIYA', 'MAHESH', 'PRABHUBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '30-Oct-91', 'Regular', '8128660911', 'mpjakhaniya@gmail.com', '', 'ME/M.TECH', '', '25-Sep-67', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(3136, 'GP, Ahmedabad', '25', 'Mr.', 'KANSAGRA', 'PRAGJI', 'BACHUBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1992-04-21', 'Regular', '9426826130', 'pbk_91@yahoo.co.in', '', 'ME/MTech', '25', '1966-06-01', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3137, 'GP, Ahmedabad', '25', 'Mr.', 'KHUNT ', ' GULAMALI ', 'RAMZANALI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1998-07-13', 'Regular', '8128291616', 'grkhunt@gmail.com', '98 Faiz e Mohammedi park, opposite to Praachina society, Juhapura Ahmedabad 380055', 'ME/M.TECH', '25', '1970-12-11', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3138, 'GP, Ahmedabad', '25', 'Mr.', 'OZA', 'AJAY', 'DAHYABHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '2011-05-23', 'Regular', '9725170458', 'adoza1856@yahoo.com', '2/5,Aalay Appartment,Nr Vastrapur bus stop,Vastrapur, Ahmedabad-15', 'ME/MTech', '25', '1977-09-04', '6000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3139, 'GP, Ahmedabad', '25', 'Ms.', 'PANDYA', 'NISHA', 'CHETAN', 'Female', 'Lecturer', 6, 'Mechanical Engg.', 25, '04-Jul-98', 'Regular', '9426352574', 'nishacryo8@gmail.com', '', 'ME/M.TECH', '', '08-Jan-75', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3140, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'AMIT', 'MANUBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '08-Nov-16', 'Regular', '9426355443', 'hiamit24@gmail.com', '', 'BE/B.TECH', '', '24-Sep-75', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3141, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'JAY', 'KANUBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '28-Oct-16', 'Regular', '8460125014', 'pateljaiy@gmail.com', '', 'ME/M.TECH', '', '08-Dec-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3142, 'GP, Ahmedabad', '25', 'Ms.', 'PATEL', 'JIKISHABEN', 'RAKESHKUMAR', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1998-07-03', 'Regular', '9824063572', 'jigishapreksha@gmail.com', '15,Badrinath Soc,Near Vaibhav Laxmi temple, Ghodasar Highway ,Ahmedabad - 50', 'ME/MTech', '25', '1976-04-22', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3143, 'GP, Ahmedabad', '25', 'Mr.', 'PATEL', 'SANJAYKUMAR', 'JAYANTIBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '2016-10-28', 'Regular', '8866127508', 'engineersanjay2012@gmail.com', 'A/5, Parth Appartment opp Thakkarbapanagar brts stand,Thakkarbapanagar, Ahmedabad', 'ME/M.TECH', '25', '1989-11-13', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3144, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'CHIRAG', 'JAYANTILAL', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1998-07-16', 'Regular', '9426526465', 'chiragshah72@rediffmail.com', 'A-104, SWAGAT RAINFOREST-1,, KUDASAN, GANDHINAGAR', 'ME/MTech', '25', '1972-05-11', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3145, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'PINKESH', 'RAMESHCHANDRA', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '25-Sep-96', 'Regular', '9825472703', 'pinkeshrshah@gmail.com', '', 'DOCTORATE', '', '09-Mar-72', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3146, 'GP, Ahmedabad', '25', 'Mr.', 'SHAH', 'PIYUSHKUMAR', 'BHUDARLAL', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1992-04-30', 'Regular', '8128660904', 'pbshah3007@gmail.com', '', 'ME/MTech', '25', '1968-07-30', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3147, 'GP, Ahmedabad', '25', 'Mr.', 'SUNDARANI', 'SIRAJALI', 'HAIDARALI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '01-May-92', 'Regular', '9227200147', 'gpasiraj@yahoo.com', '', 'ME/M.TECH', '', '21-Feb-68', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3148, 'GP, Ahmedabad', '25', 'Mr.', 'SURANI', 'KAPILKUMAR', 'KANJIBHAI', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '04-Mar-13', 'Contractual', '9724169404', 'kksurani71@gmail.com', '', 'BE/B.TECH', '', '06-Aug-91', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3149, 'GP, Ahmedabad', '25', 'Mr.', 'THAKKAR', 'HEMINKUMAR', 'VARDHILAL', 'Male', 'Lecturer', 6, 'Mechanical Engg.', 25, '1997-09-04', 'Regular', '9427678451', 'heminkumar@gmail.com', '', 'ME/MTech', '25', '1970-02-08', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3701, 'GP, Ahmedabad', '25', 'Ms.', 'LAKHATARIYA', 'DUHITA', 'BHIKHUBHAI', 'Female', 'Lecturer', 6, 'Physics', 28, '2011-06-27', 'Regular', '9725201631', 'duhita.167@gmail.com', '6/B DIWAKAR SOCIETY, NR NID,PALDI AHMEDABAD', 'Msc', '28', '1985-07-16', '5400', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3702, 'GP, Ahmedabad', '25', 'Ms.', 'Makwana', 'Dhara', 'Bharatbhai', 'Female', 'Lecturer', 6, 'Physics', 28, '06-May-11', 'Regular', '9428248350', 'dharamakwana168@yahoo.com', '', 'ME/M.TECH', '', '16-Aug-86', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3703, 'GP, Ahmedabad', '25', 'Mr.', 'SHARMA', 'PANKAJKUMAR', 'MAHESH', 'Male', 'Lecturer', 6, 'Physics', 28, '27-Jan-12', 'Regular', '9662887886', 'pks.phd14@gmail.com', '', 'Doctorate', '', '04-Apr-80', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3746, 'GP, Ahmedabad', '25', 'Mr.', 'AMIN', 'AJAYKUMAR', 'SURENDRABHAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '08-Sep-04', 'Regular', '9426044254', 'ajayamin2000@yahoo.co.in', '', 'ME/M.TECH', '', '26-Nov-74', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3747, 'GP, Ahmedabad', '25', 'Mr.', 'BHINDARWALA', ' ALIASGAR', ' ABBASBHAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '03-Mar-97', 'Regular', '9427075752', 'alibwala@gmail.com', '', 'BE/B.TECH', '', '03-May-73', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3748, 'GP, Ahmedabad', '25', 'Mr.', 'JOSHI', 'DARSHAN', 'BALVANTRAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '04-Jun-94', 'Regular', '9426346240', 'dbjoshi15469@yahoo.co.in', '', 'ME/M.TECH', '', '15-Apr-69', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3749, 'GP, Ahmedabad', '25', 'Mr.', 'OZA ', 'BHASKARKUMAR', 'ISHWARBHAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '2000-12-01', 'Regular', '9426594686', 'bhaskaroza@yahoo.com', '109, Sector-6, Chanakyapuri, Ghatlodia, Ahmedabad', 'ME/MTech', '29', '1976-03-23', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3750, 'GP, Ahmedabad', '25', 'Mr.', 'PANCHAL', 'BHARAT', 'JAYNTIBHAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '25-Aug-92', 'Regular', '9426043789', 'pbj6868@yahoo.com', '', 'BE/B.TECH', '', '06-Aug-68', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3751, 'GP, Ahmedabad', '25', 'Mr.', 'RAVAL', 'TUSHAR', 'AMRUTLAL', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '1991-07-25', 'Regular', '9228766860', 'taravalgp@yahoo.co.in', '36, Narayan Bunglows-1, Opp sola bhagwat, s g highway, gota, ahmedabad-380060', 'ME/MTech', '29', '1967-07-09', '9000', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3752, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH', 'GNANESHWARY', 'DUSHYANTKUMAR', 'Female', 'Lecturer', 6, 'Plastic Engg.', 29, '09-Aug-90', 'Regular', '9427711427', 'dipakuchat@yahoo.co.in', '', 'Doctorate', '', '11-Jul-67', '', 'DIPLOMA', '2', 3, 0, '0', 0, '0'),
(3753, 'GP, Ahmedabad', '25', 'Ms.', 'SHAH ', 'PURVI', 'BIPINCHANDRA', 'Female', 'Lecturer', 6, 'Plastic Engg.', 29, '12-Nov-09', 'Contractual', '8469016611', 'trust_purvi@yahoo.co.in', '', 'BE/B.TECH', '', '12-Jan-85', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3754, 'GP, Ahmedabad', '25', 'Mr.', 'SUVAGYA', 'NIRAVKUMAR', 'CHUNIBHAI', 'Male', 'Lecturer', 6, 'Plastic Engg.', 29, '07-Feb-09', 'Adhoc', '9924321515', 'suvagya2005@yahoo.co.in', '', 'BE/B.TECH', '', '01-Jun-87', '', 'DIPLOMA', '2', 7, 0, '0', 0, '0'),
(3917, 'GP, Ahmedabad', '25', 'Mr', 'Modi', 'Partha', 'Bipin', 'Male', 'Lecturer', 6, 'Instrumentation & Control', 22, '2018-06-08', 'Regular', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', '2018-06-01', 'NULL', 'DIPLOMA', '2', 0, 1459, '07/06/2018 23:24:35', 0, 'NULL'),
(3918, 'GP, Ahmedabad', '25', 'Mr', 'Shah', 'Bhavya', 'kumar', 'Male', 'Super Admin', 6, 'Instrumentation & Control', 6, '2018-06-09', 'Adhoc', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', '2018-06-01', 'NULL', 'Degree', '2', 6, 1459, '08/06/2018 23:02:59', 1459, '08/06/2018 23:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `role_mst`
--

CREATE TABLE `role_mst` (
  `Role_Id` int(11) NOT NULL,
  `Role_Name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_mst`
--

INSERT INTO `role_mst` (`Role_Id`, `Role_Name`) VALUES
(0, 'SuperUser'),
(1, 'Admin'),
(2, 'Principal'),
(3, 'HOD'),
(4, 'DeputyDirector'),
(5, 'JointDirector'),
(6, 'SuperAdmin'),
(7, 'Faculty'),
(8, 'Section Officer'),
(9, 'Under Secretary'),
(10, 'Deputy Secretary');

-- --------------------------------------------------------

--
-- Table structure for table `shiftallocation`
--

CREATE TABLE `shiftallocation` (
  `Shift_Allocation_Id` int(11) NOT NULL,
  `Reg_Id` int(11) DEFAULT NULL,
  `Shift_Id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shiftallocation`
--

INSERT INTO `shiftallocation` (`Shift_Allocation_Id`, `Reg_Id`, `Shift_Id`) VALUES
(1, 1, 1),
(2, 2479, 1),
(3, 2534, 1),
(4, 2528, 2),
(5, 2535, 2),
(6, 3, 1),
(7, 2, 1),
(8, 4, 1),
(10, 3918, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shift_mst`
--

CREATE TABLE `shift_mst` (
  `sh_id` int(11) NOT NULL,
  `ShiftName` varchar(20) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift_mst`
--

INSERT INTO `shift_mst` (`sh_id`, `ShiftName`, `StartTime`, `EndTime`) VALUES
(1, 'Morning', '14:50:00', '18:29:36'),
(2, 'Morning2', '11:13:00', '19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `Enrollment` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inst_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`Enrollment`, `name`, `inst_id`, `department_id`, `semester`, `address`) VALUES
('155690693003', 'Bhavya', 25, 22, 4, '204, Redwood, Ahemdabad'),
('155609693027', 'Malav', 25, 22, 2, '502 Bodhi Apartment, Navsari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`A_id`);

--
-- Indexes for table `attendance_auth`
--
ALTER TABLE `attendance_auth`
  ADD PRIMARY KEY (`AA_ID`);

--
-- Indexes for table `branch_mst`
--
ALTER TABLE `branch_mst`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `bss_id_mst`
--
ALTER TABLE `bss_id_mst`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `bss_mapping`
--
ALTER TABLE `bss_mapping`
  ADD PRIMARY KEY (`b_m_id`);

--
-- Indexes for table `fac_attendance`
--
ALTER TABLE `fac_attendance`
  ADD PRIMARY KEY (`FA_ID`);

--
-- Indexes for table `inst_mst`
--
ALTER TABLE `inst_mst`
  ADD PRIMARY KEY (`inst_id`),
  ADD KEY `FK2115AA15C2E21119` (`inst_type_id`),
  ADD KEY `FK2115AA154491164E` (`org_id`);

--
-- Indexes for table `inst_type_mst`
--
ALTER TABLE `inst_type_mst`
  ADD PRIMARY KEY (`inst_type_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`LOGINID`);

--
-- Indexes for table `map_inst_branch`
--
ALTER TABLE `map_inst_branch`
  ADD PRIMARY KEY (`map_id`),
  ADD KEY `FK10F61B587FAB6388` (`branch_id`),
  ADD KEY `FK10F61B587753CD00` (`inst_id`);

--
-- Indexes for table `org_mst`
--
ALTER TABLE `org_mst`
  ADD PRIMARY KEY (`org_id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`REGID`),
  ADD KEY `FKB94F3CD9671C08A0` (`Role_Id`);

--
-- Indexes for table `role_mst`
--
ALTER TABLE `role_mst`
  ADD PRIMARY KEY (`Role_Id`);

--
-- Indexes for table `shiftallocation`
--
ALTER TABLE `shiftallocation`
  ADD PRIMARY KEY (`Shift_Allocation_Id`),
  ADD KEY `Reg_Id` (`Reg_Id`),
  ADD KEY `Shift_Id` (`Shift_Id`);

--
-- Indexes for table `shift_mst`
--
ALTER TABLE `shift_mst`
  ADD PRIMARY KEY (`sh_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `A_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `attendance_auth`
--
ALTER TABLE `attendance_auth`
  MODIFY `AA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bss_id_mst`
--
ALTER TABLE `bss_id_mst`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `bss_mapping`
--
ALTER TABLE `bss_mapping`
  MODIFY `b_m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fac_attendance`
--
ALTER TABLE `fac_attendance`
  MODIFY `FA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `LOGINID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `shiftallocation`
--
ALTER TABLE `shiftallocation`
  MODIFY `Shift_Allocation_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `shift_mst`
--
ALTER TABLE `shift_mst`
  MODIFY `sh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inst_mst`
--
ALTER TABLE `inst_mst`
  ADD CONSTRAINT `FK2115AA154491164E` FOREIGN KEY (`org_id`) REFERENCES `org_mst` (`org_id`),
  ADD CONSTRAINT `FK2115AA15C2E21119` FOREIGN KEY (`inst_type_id`) REFERENCES `inst_type_mst` (`inst_type_id`);

--
-- Constraints for table `map_inst_branch`
--
ALTER TABLE `map_inst_branch`
  ADD CONSTRAINT `FK10F61B587753CD00` FOREIGN KEY (`inst_id`) REFERENCES `inst_mst` (`inst_id`),
  ADD CONSTRAINT `FK10F61B587FAB6388` FOREIGN KEY (`branch_id`) REFERENCES `branch_mst` (`branch_id`);

--
-- Constraints for table `registration`
--
ALTER TABLE `registration`
  ADD CONSTRAINT `FKB94F3CD9671C08A0` FOREIGN KEY (`Role_Id`) REFERENCES `role_mst` (`Role_Id`);

--
-- Constraints for table `shiftallocation`
--
ALTER TABLE `shiftallocation`
  ADD CONSTRAINT `shiftallocation_ibfk_1` FOREIGN KEY (`Reg_Id`) REFERENCES `registration` (`REGID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
