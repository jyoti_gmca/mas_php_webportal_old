<?php
$status=null; 
 $connect = mysqli_connect("localhost", "root", "", "cteict");  
 $query = "SELECT Status FROM attendance ";  
 $result = mysqli_query($connect,$query);  
$data = array(
    'cols' => array(
      //  array('type' => 'string', 'label' => 'Faculties'),
        //array('type' => 'string', 'label' => 'Present'),
        array('type' => 'string', 'label' => 'Absent')
    ),
    'rows' => array()
);
while($row = mysqli_fetch_array($result)) {
	//$status=null;
	if($row[0]==1)
	{
		$status="Present";
	}
	else
	{
		$status="Absent";
	}
    
    // parse start and finish into strings $startDate and $finishDate with this format:
    // "Date(year, month, day, hour, minute, second, millisecond)"
    // where month is zero-indexed (eg, January is 0 not 1), and hour, minute, second, and millisecond are optional
    $data['rows'][] = array('c' => array(
     //   array('v' => $row['goalName']),
        array('v' => $status),
       // array('v' => $finishDate)
    ));
}
?>
<script src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}">
function drawChart() {
    var container = document.getElementById('example3.1');
    var chart = new google.visualization.Timeline(container);
    
    var data = new google.visualization.DataTable(<?php echo json_encode($data, JSON_NUMERIC_CHECK); ?>);

    
    var options = {
        title: 'Progress',
        is3D: true
    };
    chart.draw(data, options);
}
</script>