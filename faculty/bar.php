<?php
$servername = "localhost";
							$username = "root";
							$password = "";
							$dbname="cteict";
							$conn = mysqli_connect($servername, $username, $password , $dbname);


$query = "SELECT DISTINCT DATE, count(*) as number  FROM attendance GROUP BY DATE";  
$res = $conn->query($query);
?>
<!DOCTYPE html>
 <html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['DATE','number'],
          <?php
            while($row=$res->fetch_assoc())
            {
              echo "['".$row['DATE']."',".$row['number']."],";
              
            }
            ?>
        ]);

        var options = {
          title: 'Line Chart'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <h1>line chart</h1>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>
</html>
