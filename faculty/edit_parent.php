<?php
session_start();
$login=null;
$owner=null;
if(isset($_SESSION["login_user"]))
{
	$login=$_SESSION["login_user"];
}

else
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
}
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Attendance Admin Panel</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="../assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="../assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
	<link href="themify-icons-plugin/assets/themify-icons.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            
                            <!-- Light Logo icon -->
                            <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         <img src="../assets/images/background/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
						<?php
						if($login=="Mohit")
						{
                            echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/1.jpg" alt="user" class="profile-pic m-r-10" />Mohit Tanwani</a>';
						}
						elseif($login=="Vishal")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Vishal.jpg" alt="user" class="profile-pic m-r-10" />Vishal Tanwani</a>';
                        }    
						else if($login=="Pratibha")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Pratibha.png" alt="user" class="profile-pic m-r-10" />Pratibha Hotwani</a>';
                        }
						else if($owner=="Chetan")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Chetan.png" alt="user" class="profile-pic m-r-10" />Chetan Mahajan</a>';
                        }
						else
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Root.png" alt="user" class="profile-pic m-r-10" />Root Login</a>';

						}
						?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="index.php" aria-expanded="true"><i class="ti-dashboard"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
						<!-- <a class="btn btn-secondary btn-sm dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
                        <?php
						
						?>
						<li> <a class="waves-effect waves-dark" data-toggle="dropdown" class="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Students</span></a>
						
						<ul>
						
						
						<li><a href='Add_Users_1.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add student</a></li>
					
						
						<li><a href="All_Users_1.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All students</a></li>
						</ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Faculty</span></a>
					<ul>
					
						<li><a href='Add_Faculty.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Faculty</a></li>
						
						<li><a href="View_All_Faculty.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All Faculty</a></li>
					</ul>
					 <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Parent</span></a>
					<ul>
					
					<li><a href='Add_Parent.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Parent</a></li>
						
						<li><a href="View_All_Parent.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All Parents</a></li>
					</ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-check-box"></i><span class="hide-menu">Attendance</span></a>
                <ul>
					
					
						<li><a href='View_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Sub_Wise_attendance</a></li>
							
								<li><a href='Monthly_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Monthly_Attendance</a></li>
									<li><a href='Daily_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Daily_Attendance</a></li>
									<li><a href='Faculty_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Faculty_Attendance</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-calendar"></i><span class="hide-menu">TimeTable</span></a>
                    <ul>
					
					
						<li><a href='timetable.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add TimeTable</a></li>
						<li><a href='View_Timetable.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View TimeTable</a></li>
						
						</ul>
						
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-blackboard"></i><span class="hide-menu">Days</span></a>
                    <ul>
					
					
						<li><a href='Add_day.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Days</a></li>
						<li><a href='View_Day.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Days</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-timer"></i><span class="hide-menu">Time</span></a>
                    <ul>
					
					
						<li><a href='Add_Time.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Time</a></li>
						<li><a href='View_Time.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Time</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-harddrives"></i><span class="hide-menu">Semester</span></a>
                    <ul>
					
					
						<li><a href='Add_Semester.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Semester</a></li>
						<li><a href='View_Semester.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Semester</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-panel"></i><span class="hide-menu">Division</span></a>
                    <ul>
					
					
						<li><a href='Add_Division.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Division</a></li>
						<li><a href='View_Division.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Division</a></li>
						
						</ul>
						
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-info-alt"></i><span class="hide-menu">Reports</span></a>
						<ul>
			
						
						<li><a href="Attendance_Reports.php"><i class="ti-bar-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;Attendance Reports</a></li>
					</ul>
					</li>
					
                    </ul>
                    
                    
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Manage"><i class="ti-settings"></i></a>
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Feedbacks"><i class="ti-comments"></i></a>
                <!-- item--><a href="logout.php" class="link" data-toggle="tooltip" title="Logout"><i class="ti-power-off"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
        <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Parent</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Parent</a></li>
                            <li class="breadcrumb-item active">Edit Parent</li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
				<?php
				$servername="localhost";
$username = "root";
$password = "";
$dbname="cteict";
$conn =mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed:" . $conn->connect_error);
} 
				$aa=$_GET['value'];
				$query = "select * from parent_details where PARENT_ID='$aa'";
				$sql=mysqli_query($conn,$query);
				$a=null;
				$b=null;
				$c=null;
				$d=null;
				$e=null;
				$f=null;
				$g=null;
				
				while($row=mysqli_fetch_array($sql))
				{
					$a=$row[0];
					$b=$row[1];
					$c=$row[2];
					$d=$row[3];
					$e=$row[4];
					$f=$row[5];
					$g=$row[6];
					
					
				}
				
				?>
				<div class="card">
                <div class="card-block">
				<div class="table-responsive">
							<form class="form-vertical" action="" method="POST" >
							
								 
								 <div class="form-group">
                                        <label class="col-md-12">Parent_ID</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n1" value="<?php echo $a;?>">
                                        </div>
                                  </div>
							
                                <div class="form-group">
                                  <label class="col-md-12" >Parent_Name</label>
                                    <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" name="n2" value="<?php echo $b;?>">
                                    </div>
                                </div>
                                     
                                     <div class="form-group">
                                        <label class="col-md-12">Parent_Mobile</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n3" value="<?php echo $c;?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12">Parent_Password</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n4" value="<?php echo $d;?>">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-12">Parent_Email</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n5" value="<?php echo $e;?>">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-12">Parent_DOB</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n6" value="<?php echo $f;?>">
                                        </div>
                                    </div>
                                    
							 <div class="form-group">
                                  <label class="col-md-12" >Parent_Address</label>
                                    <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" name="n7" value="<?php echo $g;?>">
                                    </div>
                                </div>
								 
									<div class="form-group">
                                    
                                     <div class="col-md-12">
                                       <center> <button class="btn btn-success" name="sub">Update Parent</button></center>
										</div>
									</div>
<?php
$servername="localhost";
$username = "root";
$password = "";
$dbname="cteict";
$conn =mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed:" . $conn->connect_error);
} 
if(isset($_POST['sub']))
{
$parid=$_POST['n1'];
$parname=$_POST['n2'];
$parmob=$_POST['n3'];
$parpass=$_POST['n4'];
$parmail=$_POST['n5'];
$pardob=$_POST['n6'];
$paradd=$_POST['n7'];


$query = "UPDATE `parent_details` SET `PARENT_NAME`='$parname',`PARENT_MOBILE`=$parmob,`PARENT_PASSWORD`='$parpass',`PARENT_EMAIL`='$parmail',`PARENT_DOB`='$pardob',`PARENT_ADDRESS`='$paradd' WHERE PARENT_ID='$aa'";
if(mysqli_query($conn,$query))
{
echo '<div class="alert alert-success">success!! User Updated</div>';
}	
else
{
echo '<div class="alert alert-danger">Some Error Occured'.mysqli_error($conn).'</div>';
}
mysqli_close($conn);
}

		?>
                </tbody>
				   </table>
					</div>  
                </div>   
            </div>   
					
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
			
        </div>
	</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
			</div>
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2018  , GLS University
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    
	</div>
	</div>
	
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>

</body>

</html>
