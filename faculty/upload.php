<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.kairos.com/enroll");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);

		$path = 'uploads/'.basename( $_FILES["fileToUpload"]["name"]);
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		$nameofimage=basename( $_FILES["fileToUpload"]["name"]);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{
		  \"image\": \"$base64\",
		  \"subject_id\": \"$nameofimage\",
		  \"gallery_name\": \"faculty\"
		}");

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "app_id: 4ba7ccd1",
		  "app_key: b129effb1951d971cbc469185dcd6d79"
		));

		$response = curl_exec($ch);
		curl_close($ch);

		var_dump($response);

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
