<?php
session_start();
$director=0;
$principal=0;
$HOD=0;
if(isset($_SESSION["login_user"]))
{
    $login=$_SESSION["login_user"];
}
else
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
}
$servername = "localhost";
$username = "root";
$password = "";
$dbname="cteict";
$institutename=null;
$instituteid=0;
$branchname=null;
$branchid=0;
$roleid=0;
$status=null; 
$conn = mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed: " . $conn->connect_error);
}
$sql="select Role_id,College_id,Department_Id from registration where REGID IN(select Reg_id from login where LID='$login')";
$exe=mysqli_query($conn,$sql);
while($row=mysqli_fetch_array($exe))
{
     $roleid=$row[0];
	 $instituteid=$row[1];
	 $branchid=$row[2];
}
if($roleid==6)
{
    $director=1;
    $query = "SELECT SUM( CASE WHEN Status = '1' THEN 1 ELSE 0 END ) as present,SUM( CASE WHEN Status = '0' THEN 1 ELSE 0 END ) as absent,
	inst_mst.inst_name,Date as date FROM attendance 
	INNER JOIN registration ON attendance.Reg_id = registration.REGID
    INNER JOIN inst_mst ON registration.College_Id = inst_mst.inst_id GROUP BY Date";  
    $result = mysqli_query($conn,$query);  

}
if($roleid==2)
{
	$principal=1;
	$query = "SELECT SUM( CASE WHEN Status = '1' THEN 1 ELSE 0 END ) as present,SUM( CASE WHEN Status = '0' THEN 1 ELSE 0 END ) as absent,
	inst_mst.inst_id,Date as date FROM attendance 
	INNER JOIN registration ON attendance.Reg_id = registration.REGID
    INNER JOIN inst_mst ON registration.College_Id = inst_mst.inst_id GROUP BY Date 
    HAVING inst_mst.inst_id=$instituteid";
    // $query = "SELECT SUM( CASE WHEN Status = '1' THEN 1 ELSE 0 END ) as present,
    // SUM( CASE WHEN Status = '0' THEN 1 ELSE 0 END ) as absent,inst_mst.inst_name,Date as date
    // FROM attendance 
          // INNER JOIN registration ON attendance.Reg_id = registration.REGID
          // INNER JOIN inst_mst ON registration.College_Id = inst_mst.inst_id
    // WHERE attendance.Reg_id IN(select Reg_id from login where LID='$login')
    // GROUP BY Date";  
    $result = mysqli_query($conn,$query);  
}
if($roleid==3)
{
	$HOD=1;
	$query = "SELECT SUM( CASE WHEN Status = '1' THEN 1 ELSE 0 END ) as present,SUM( CASE WHEN Status = '0' THEN 1 ELSE 0 END ) as absent,
	inst_mst.inst_id,Date as date FROM attendance 
	INNER JOIN registration ON attendance.Reg_id = registration.REGID
    INNER JOIN inst_mst ON registration.College_Id = inst_mst.inst_id GROUP BY Date 
    HAVING inst_mst.inst_id=$instituteid";
    // $query = "SELECT SUM( CASE WHEN Status = '1' THEN 1 ELSE 0 END ) as present,
    // SUM( CASE WHEN Status = '0' THEN 1 ELSE 0 END ) as absent,inst_mst.inst_name,Date as date
    // FROM attendance 
          // INNER JOIN registration ON attendance.Reg_id = registration.REGID
          // INNER JOIN inst_mst ON registration.College_Id = inst_mst.inst_id
          // INNER JOIN branch_mst ON registration.Designation_Id = branch_mst.branch_id
    // WHERE attendance.Reg_id IN(select Reg_id from login where LID='$login')
    // GROUP BY Status";  
    $result = mysqli_query($conn,$query);  
}
if($roleid==7)
{
    $query = "SELECT count(A_id) AS number, Status, Date FROM `attendance`
    WHERE attendance.Reg_id IN(select Reg_id from login where LID='$login')
    GROUP BY Status"; 
    $result = mysqli_query($conn,$query);  
}
?>
 <!-- old code -->
<!DOCTYPE html>  
<html>  
<head>  
<title>Faculty Attendance</title>  
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<br/>
<center><div class="col-md-12">
<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
<input type="text" name="n1" id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 6px outset cyan; width: 20%">
&nbsp;
<input type="submit" class="btn btn-info" name="n2" value="Show" width="18%"/>&nbsp; &nbsp; 
<?
if(isset($_POST['n2']))
{

}
?>
</div></center>   
<?php 
    if($roleid !=7){

    
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
<script type="text/javascript">  
           google.charts.load('current', {'packages':['corechart']});  
           google.charts.setOnLoadCallback(drawChart);  
           function drawChart()  
           {  
                var data = google.visualization.arrayToDataTable([  
                          ['Date','Present','Absent'],  
                          <?php  
                          while($row = mysqli_fetch_array($result))  
                          {  
                            
                            echo "['".$row['date']."', ".$row["present"].",".$row["absent"]."],";   
                          }  
                          ?>  
                     ]);  
                var options = {  
                      title: 'Total Attendance ',  
                    width: 1000,
                     height: 490,
                    // bar: {groupWidth: "95%"},
                    // legend: { position: "none" },
					// colors: ['#ABEBC6','#617FEE'],
                    isStacked:true
                     };  
                var chart = new google.visualization.BarChart(document.getElementById('barchart_values'));  
                chart.draw(data, options);  
				
           }  
</script>
<?php
    }
    else{
    
?> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
<script type="text/javascript">  
           google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
      
           function drawChart()  
           {  
                var data = google.visualization.arrayToDataTable([
                          ['Element', 'Density', { role: 'style' }, { role: 'annotation' } ],  
                          <?php  
						  $cnt1=0;
						  $cnt2=0;
                          while($row = mysqli_fetch_array($result))  
                          {  
							if($row['Status']==0)
							{
								$cnt1=intval($row[0]);
								$status1="Total Absent";
							}
							else
							{
								$cnt2=intval($row[0]);
								$status2="Total Present";
							}
                          }  
						  echo "['$status1', $cnt1,'#FB0226', 'Absent  '],";
						  echo "['$status2', $cnt2,'#617FEE', 'Present  ']";
                          ?>  
                     ]);  
                var options = {  
                      title: 'Total Attendance ',  
                    //   is3D:true,  
                    //   pieHole: 0.4, 
                    width: 1000,
                    height: 490,
                    bar: {groupWidth: "95%"},
                    legend: { position: "none" },
					colors: ['#317FEE','#317FEE']
                     };  
                var chart = new google.visualization.BarChart(document.getElementById('barchart_values'));  
                chart.draw(data, options);  
				
           }  
</script>  
<?php
    }

?>
</head>  
<body>  
           <br /><br />  
           <div style="width:900px;">  
                
                <br />  
                <div id="barchart_values" style="width: 500px; height: 500px;"></div>  
            
           </div>  
      </body>  
 </html>  

 <script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange input').html(start.format('MM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>