<?php
$servername="localhost";
$username = "root";
$password = "";
$dbname="cteict";
$query = "SELECT DISTINCT DATE, count(*) as number  FROM attendance GROUP BY DATE ";  
$conn =mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed:" . $conn->connect_error);
} 
$res = $conn->query($query);
?>
<!DOCTYPE html>
<html>



    <body>

     
			    <!-- dashboard content write below-->
			<div id="piechart" style="width: 980px; height: 520px; padding-left:20px;"></div>
        <!-- jQuery  -->
      
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['DATE','number'],
	<?php
	while($row=$res->fetch_assoc())
	{
		echo "['".$row['DATE']."',".$row['number']."],";
		
	}
	?>
        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

    </body>
</html>