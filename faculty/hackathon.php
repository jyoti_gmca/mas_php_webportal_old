<!DOCTYPE HTML>
<html>
<head>  
<style>
 #chartContainer{
    position: absolute;
    right: 100px;
    top: 100px;
}
 #Container{
    position: absolute;
    left: 100px;
    top: 100px;
}
#hartContainer{
    position: absolute;
    left: 100px;
    top: 450px;
}
#break{
    position: absolute;
    left: 100px;
    top: 850px;
}

</style>

</head>
<body>
<div  id="chartContainer" style="height: 200px; width: 30%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<div id="Container" style="height: 300px; width: 30%;"></div>
<div id="hartContainer" style="height: 300px; width: 80%;"></div>

 <script>
var d = 12;	
var e = 6;	
var f = 18;


window.onload = function() {
var chart = new CanvasJS.Chart("chartContainer", {
					title:{
						text: "Current Attedance Analysis"
					},
					data: [
					{
						// Change type to "doughnut", "line", "splineArea", etc.
						type: "column",
						dataPoints: [
							{ label: "Fill",  y: d  },
							{ label: "Unfill", y: e  },
							{ label: "Total", y: f  }

						]
					}
					]
				});

				chart.render();


var chart = new CanvasJS.Chart("Container", {
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	exportEnabled: true,
	animationEnabled: true,
	title: {
		text: "Percentage of Attendance"
	},
	data: [{
		type: "pie",
		startAngle: 25,
		toolTipContent: "<b>{label}</b>: {y}%",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - {y}%",
		dataPoints: [
			{ y: (d*100)/f, label: "Fill" },
			{ y: (e*100)/f, label: "Unfill" }
			
			
		]
	}]
});
chart.render();


var chart = new CanvasJS.Chart("hartContainer", {
	animationEnabled: true,
	title:{
		text: "Analisys Attendenc Record"
	},
	axisX:{
		valueFormatString: "DD MMM"
	},
	axisY: {
		title: "Number of Attendance",
		includeZero: false,
		scaleBreaks: {
			autoCalculate: true
		}
	},
	data: [{
		type: "line",
		xValueFormatString: "DD MMM",
		color: "#F08080",
		dataPoints: [
			{ x: new Date(2017, 0, 1), y: 6 },
			{ x: new Date(2017, 0, 2), y: 27 },
			{ x: new Date(2017, 0, 3), y: 18 },
			{ x: new Date(2017, 0, 4), y: 20 },
			{ x: new Date(2017, 0, 5), y: 40 },
			{ x: new Date(2017, 0, 6), y: 58 },
			{ x: new Date(2017, 0, 7), y: 34 },
			{ x: new Date(2017, 0, 8), y: 63 },
			{ x: new Date(2017, 0, 9), y: 47 },
			{ x: new Date(2017, 0, 10), y: 53 },
			{ x: new Date(2017, 0, 11), y: 69 },
			{ x: new Date(2017, 0, 12), y: 43 },
			{ x: new Date(2017, 0, 13), y: 70 },
			
		]
	}]
});
chart.render();
}




</script>
<p id="break"><br></p>
</body>
</html>