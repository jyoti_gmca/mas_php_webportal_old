<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
  <title>Attendance Admin Panel</title>
  <link href="js1/k1.css" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="js1/h1.css">
  <link rel="stylesheet" href="js1/h2.css">
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	 <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="../assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="../assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css1/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="../css/colors/blue.css" id="theme" rel="stylesheet">
  <title>Admin Login</title>
 <style type="text/css">
/* NOTE: The styles were added inline because Prefix free needs access to your styles and they must be inlined if they are on local disk! */
body 
{
  font-family: "Open Sans", sans-serif;
  height: 100vh;
  background: url("../assets/11.png") 50% fixed;
  background-size: 1500px 635px; 
  background-repeat: no-repeat;
}

*{
	box-sizing: border-box;
}

.wrapper
{
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
  background: rgba(4, 40, 68, 0.50);
}

.login 
{
  border-radius: 2px 2px 5px 5px;
  padding: 10px 20px 20px 20px;
  width: 90%;
  max-width: 320px;
  background: #ffffff;
  position: relative;
  padding-bottom: 80px;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
}

.login input 
{
  display: block;
  padding: 15px 10px;
  margin-bottom: 10px;
  width: 100%;
  border: 1px solid #ddd;
  border-radius: 2px;
  color: #ccc;
}

.login input + i.fa 
{
  color: #fff;
  font-size: 1em;
  position: absolute;
  margin-top: -47px;
  opacity: 0;
  left: 0;
 
}

.login input:focus 
{
  outline: none;
  border-color: #2196F3;
  border-left-width: 35px;
}

.login input:focus + i.fa 
{
  opacity: 1;
  left: 30px;
}

.login a 
{
  font-size: 0.8em;
  color: #2196F3;
  text-decoration: none;
}

.login .title 
{
  color: #2196F3; 
  font-size: 1.2em;
  font-weight: bold;
  margin: 0.8px 0 5px 0;
  border-bottom: 1px solid #eee;
  padding-bottom: px;
}
</style>
<script src="js1/k5.js"></script>
</head>
<body>
<div class="wrapper">
 <h3 align="center" style="color:red;"><?php echo @$_GET['success']; ?></h3>
 <h3 align="center" style="color:red"><?php echo @$_GET['invailid']; ?></h3>
 <h2 align="center" style="color:red"><?php echo @$_GET['logout']; ?></h2>
<form action="login.php" method="POST" class="login">
<font color="#2196F3"><b><h4>Login ID:</h4></b></font>
<input type="text" placeholder="Login ID"  name="txtuser" required/>
<i class="fa fa-user"></i>
<font color="#2196F3"><b><h4>Password:</h4></b></font>
<input type="password" placeholder="Password" name="txtpass" required/>
<i class="fa fa-user"></i>
<br/>
<center><button class="btn btn-success" name="sub" value="Log In"><h4><b>LOG IN</b></h4></button></center>
    
  
 </form>

</div>
<script src="js1/kk.js"></script>
</body>
</html>