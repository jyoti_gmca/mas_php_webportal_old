CREATE TABLE cricketer
(
	cri_id NUMBER PRIMARY KEY,
	cri_nm VARCHAR(20) NOT NULL,
	match_play NUMBER NOT NULL,
	tot_run NUMBER NOT NULL,
	experience NUMBER NOT NULL,
	country_nm VARCHAR(20) NOT NULL
);

-- Create Table Query
CREATE TABLE tables
(
	tid NUMBER PRIMARY KEY,
	tnm VARCHAR(15) NOT NULL,
	tcity VARCHAR(10) NOT NULL,
	tstate VARCHAR(10) NOT NULL,
	tcountry VARCHAR(10),
	tphone NUMBER NOT NULL,
	temail VARCHAR(15) 
);
--Insert Query
INSERT INTO tables 
VALUES (101,'Bhavya','Surat','Gujarat','India',7600345578,'shah@gmail.com');

INSERT INTO tables (tid,tnm,tcity,tstate,tphone)
VALUES (102,'Tej','Ahemdabad','Gujarat',1234);

--Select Query
SELECT * FROM tables;

SELECT tnm,tcity,tphone from tables;

--COMMENT Query after Work Done
COMMIT;

--Delete Query for Delete Record
DELETE FROM tables; 

--Drop Query for Delete Whole Table
DROP TABLE tables; 