<?php
session_start();
$login=null;
$owner=null;
if(isset($_SESSION["login_user"]))
{
	$login=$_SESSION["login_user"];
}

else
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
}
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Attendance Admin Panel</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="../assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="../assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
	<link href="themify-icons-plugin/assets/themify-icons.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            
                            <!-- Light Logo icon -->
                            <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         <img src="../assets/images/background/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
						<?php
						if($login=="Mohit")
						{
                            echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/1.jpg" alt="user" class="profile-pic m-r-10" />Mohit Tanwani</a>';
						}
						elseif($login=="Vishal")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Vishal.jpg" alt="user" class="profile-pic m-r-10" />Vishal Tanwani</a>';
                        }    
						else if($login=="Pratibha")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Pratibha.png" alt="user" class="profile-pic m-r-10" />Pratibha Hotwani</a>';
                        }
						else if($owner=="Chetan")
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Chetan.png" alt="user" class="profile-pic m-r-10" />Chetan Mahajan</a>';
                        }
						else
						{
							echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/Root.png" alt="user" class="profile-pic m-r-10" />Root Login</a>';

						}
						?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="index.php" aria-expanded="true"><i class="ti-dashboard"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
						<!-- <a class="btn btn-secondary btn-sm dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
                        <?php
						
						?>
						<li> <a class="waves-effect waves-dark" data-toggle="dropdown" class="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Students</span></a>
						
						<ul>
						
						
						<li><a href='Add_Users_1.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Student</a></li>
					
						
						<li><a href="All_Users_1.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All Students</a></li>
						</ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Faculty</span></a>
					<ul>
					
						<li><a href='Add_Faculty.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Faculty</a></li>
						
						<li><a href="View_All_Faculty.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All Faculty</a></li>
					</ul>
					 <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Parent</span></a>
					<ul>
					
						<li><a href='Add_Parent.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Parent</a></li>
						
						<li><a href="View_All_Parent.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View All Parents</a></li>
					</ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-check-box"></i><span class="hide-menu">Attendance</span></a>
                    <ul>
					
					
						<li><a href='View_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Sub_Wise_attendance</a></li>
							
								<li><a href='Monthly_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Monthly_Attendance</a></li>
									<li><a href='Daily_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Daily_Attendance</a></li>
									<li><a href='Faculty_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Faculty_Attendance</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-calendar"></i><span class="hide-menu">TimeTable</span></a>
                    <ul>
					
					
						<li><a href='timetable.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add TimeTable</a></li>
						<li><a href='View_Timetable.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View TimeTable</a></li>
						
						</ul>
						
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-blackboard"></i><span class="hide-menu">Days</span></a>
                    <ul>
					
					
						<li><a href='Add_day.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Days</a></li>
						<li><a href='View_Day.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Days</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-timer"></i><span class="hide-menu">Time</span></a>
                    <ul>
					
					
						<li><a href='Add_Time.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Time</a></li>
						<li><a href='View_Time.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Time</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-harddrives"></i><span class="hide-menu">Semester</span></a>
                    <ul>
					
					
						<li><a href='Add_Semester.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Semester</a></li>
						<li><a href='View_Semester.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Semester</a></li>
						
						</ul>
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-panel"></i><span class="hide-menu">Division</span></a>
                    <ul>
					
					
						<li><a href='Add_Division.php'><i class='ti-plus'></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Division</a></li>
						<li><a href='View_Division.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;View Division</a></li>
						
						</ul>
						
						<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-info-alt"></i><span class="hide-menu">Reports</span></a>
						<ul>
			
						
						<li><a href="Attendance_Reports.php"><i class="ti-bar-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;Attendance Reports</a></li>
					</ul>
					</li>
					
                    </ul>
                    
                    
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Manage"><i class="ti-settings"></i></a>
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Feedbacks"><i class="ti-comments"></i></a>
                <!-- item--><a href="logout.php" class="link" data-toggle="tooltip" title="Logout"><i class="ti-power-off"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
        <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Faculty</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Faculty</a></li>
                            <li class="breadcrumb-item active">Add_Faculty</li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
			<div class="card">
                <div class="card-block">
				<div class="table-responsive">
							<form class="form-vertical" action="" method="POST" >
							
								<div class="form-group">
                                        <label class="col-md-12">Faculty_ID</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n1">
                                        </div>
                                  </div>
							
                                <div class="form-group">
                                  <label class="col-md-12" >Faculty_Name</label>
                                    <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" name="n2">
                                    </div>
                                </div>
                                     
                                     <div class="form-group">
                                        <label class="col-md-12">Faculty_Mobile</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n3">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12">Faculty_Password</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n4">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-12">Faculty_Email</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n5">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-12">Faculty_DOB</label>
                                        <div class="col-md-12">
                                            <input type="text"  class="form-control form-control-line" name="n6">
                                        </div>
                                    </div>
                                    
							 <div class="form-group">
                                  <label class="col-md-12" >Faculty_Address</label>
                                    <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" name="n7">
                                    </div>
                                </div>
								 <div class="form-group">
                                  <label class="col-md-12" >Faculty_Gender</label>
                                    <div class="col-md-12">
                                   <select class="form-control" name="n8">
								   <option value="Select a Division" hidden>Select Gender</option>
								   <option value="Male">Male</option>
								   <option value="Female">Female</option>
								  
								   </select>
                                    </div>
                                </div>
								
									<div class="form-group">
                                    
                                     <div class="col-md-12">
                                       <center> <button class="btn btn-success" name="sub">Add Faculty</button></center>
										</div>
									</div>
									<div class="form-control" style="filter:alpha(opacity=50); opacity:0.0";>
									<?php
if(isset($_POST['sub']))
{
$facid=$_POST['n1'];
$facname=$_POST['n2'];
$facmob=$_POST['n3'];
$facpass=$_POST['n4'];
$facmail=$_POST['n5'];
$facdob=$_POST['n6'];
$facadd=$_POST['n7'];
$facgender=$_POST['n8'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname="cteict";
$conn = mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed: " . $conn->connect_error);
} 
$query = "INSERT INTO `faculty_details`(`FACULTY_ID`, `FACULTY_NAME`, `FACULTY_MOBILE`, `FACULTY_PASSWORD`, `FACULTY_EMAIL`, `FACULTY_DOB`, `FACULTY_ADDRESS`, `FACULTY_GENDER`) VALUES ('$facid','$facname',$facmob,'$facpass','$facmail','$facdob','$facadd','$facgender')";
if(mysqli_query($conn,$query))
{
echo '<div class="alert alert-success">success!!</div>';
}	
else
{
echo '<div class="alert alert-danger">Some Error Occured'.mysqli_error($conn).'</div>';
}
mysqli_close($conn);
}
?>
				</div>
                </form>
				</div>
                </div>
				</div>
                    <!-- Column -->
					
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
			
            </div>
			<footer class="footer"> <span> © 2018, Government Of Gujarat </span>
            <span style ="margin-left:50%">Terms and Services &nbsp;&nbsp;  Privacy Policies</span></footer>
                    <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="../assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 JavaScript -->
    <script src="../assets/plugins/d3/d3.min.js"></script>
    <script src="../assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="js/dashboard1.js"></script>
</body>

</html>

