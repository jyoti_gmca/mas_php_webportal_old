<?php
session_start();
$director=0;
$principal=0;
$HOD=0;
if(isset($_SESSION["login_user"]))
{
	$login=$_SESSION["login_user"];
}
else
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
}
$servername = "localhost";
$username = "root";
$password = "";
$dbname="cteict";
$institutename=null;
$instituteid=0;
$branchname=null;
$branchid=0;
$conn = mysqli_connect($servername, $username, $password , $dbname);
if ($conn==false)
{
    die("Connection failed: " . $conn->connect_error);
}
$sql="select Role_id from registration where REGID IN(select Reg_id from login where LID='$login')";
$exe=mysqli_query($conn,$sql);
while($row=mysqli_fetch_array($exe))
{
	 $roleid=$row[0];
}
if($roleid==6)
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
	$director=1;
}
if($roleid==2)
{
	echo " <h2>Login First </h2>";
	header("location:index_form.php");
	$principal=1;
	$sql="select College_Name from registration where REGID IN(select Reg_id from login where LID='$login')";
	$exe=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_array($exe))
	{
	$institutename=$row[0];
	}
	$sql="select inst_id from inst_mst where inst_name='$institutename'";
	$exe=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_array($exe))
	{
	$instituteid=$row[0];
	}
	
}
if($roleid==3)
{
	$HOD=1;
	$sql="select College_Id from registration where REGID IN(select Reg_id from login where LID='$login')";
	$exe=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_array($exe))
	{
	$instituteid=$row[0];
	$sql1="select inst_name from inst_mst where inst_id=$row[0]";
	$exe1=mysqli_query($conn,$sql1);
	while($row=mysqli_fetch_array($exe1))
	{
		$institutename=$row[0];
	}
	}
	$sql="select DepartmentName from registration where REGID IN(select Reg_id from login where LID='$login')";
	$exe=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_array($exe))
	{
	$branchname=$row[0];
	$sql="select branch_id from branch_mst where branch_type='$branchname'";
	$exe=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_array($exe))
	{
	$branchid=$row[0];
	}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Attendance Admin Panel</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="../assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="../assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
	<link href="themify-icons-plugin/assets/themify-icons.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.min.css" rel="stylesheet">
    <link href="themify-icons-plugin/assets/themify-icons-frontend.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
$(document).ready(function(){
    $('#institute_name').on('change',function()
	{
        var categoryID = $(this).val();
        if(categoryID)
		{
            $.ajax(
			{
                type:'POST',
                url:'ajaxData.php',
                data:'Category_id='+categoryID,
                success:function(html)
				{
                    $('#branch').html(html);
                }
            }); 
        }
    });
});
</script>
	<script type="text/javascript">
$(document).ready(function(){
    $('#branch').on('change',function()
	{
        var os_id = "Electronics";//$(this).val();
        if(os_id)
		{
            $.ajax(
			{
                type:'POST',
                url:'branch_php',
                data:'os_id='+os_id,
                success:function(html)
				{
                    $('#get1').html(html);
                }
            }); 
        }
    });
});
</script>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            
                            <!-- Light Logo icon -->
                            
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         <img src="../assets/images/background/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                         <?php
						$sql="select Person_Name,Surname from registration where REGID IN(select Reg_Id from login where LID='$login')";
						$exe=mysqli_query($conn,$sql);
						while($row=mysqli_fetch_array($exe))
						{
						$firstname=$row[0];
						//echo $firstname;
						$lasttname=$row[1];
						//echo $lasttname;
	 					}
$fullname="$firstname  $lasttname";
$fullname;	
						?>
                        <li class="nav-item dropdown">
						<?php
						$sql="select Reg_Id from login where LID='$login'";
						$exe=mysqli_query($conn,$sql);
						while($row=mysqli_fetch_array($exe))
						{
							$id=$row[0];
						}
						$ids=(string)$id;
						$idss="file/uploads/$id.jpg";
						//echo $idss;
							$locations="file/uploads/$id.jpg";
                            echo '<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="pages-profile.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src='.$idss.' alton ="icon" id="pic" class="profile-pic m-r-10" />'.$fullname.'</a>';
							//echo '<img src='.$idss.'>';
						?>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
       <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
					   <?php
					if($director==1 OR $principal==1)
			{
echo '<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Faculty</span></a>
<ul>
<li><a href="faculty_reg.php"><i class="ti-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Faculty</a></li>
<li><a href="image_upload.php"><i class="ti-gallery"></i>&nbsp;&nbsp;&nbsp;&nbsp;Image Upload</a></li>
</ul>
<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-timer"></i><span class="hide-menu">Shift</span></a>
<ul>
<li><a href="Shift_View.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View Shift</a></li>
<li><a href="view_allocation.php"><i class="ti-eye"></i>&nbsp;&nbsp;&nbsp;&nbsp;View Shift Allocation</a></li>
<li><a href="shift_allocation.php"><i class="ti-control-shuffle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Add Shift Allocation</a></li>
</ul>
</li>
<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-signal"></i><span class="hide-menu">BSS</span></a>
<ul>
<li><a href="Add_Bss.php"><i class="ti-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;Add BSS</a></li>

</ul>';
}
?>
                   <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-check-box"></i><span class="hide-menu">Attendance</span></a>
					<ul>
					<?php
					if($director==1)
						{
						echo"<li><a href='Institute_at.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Institute Attendance</a></li>
					    <li><a href='View_Attendance.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Branch Attendance</a></li>
						<li><a href='Faculty_at.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Faculty Attendance</a></li>";
						}
					if($principal==1)
						{
						echo "<li><a href='Branch_Principal_reports.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Branch Attendance</a></li>
						<li><a href='Faculty_at.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Faculty Attendance</a></li>";
						}
					if($HOD==1)
					{
						echo "<li><a href='HOD_reports.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Branch Attendance</a></li>
						<li><a href='Faculty_at.php'><i class='ti-eye'></i>&nbsp;&nbsp;&nbsp;&nbsp;Faculty Attendance</a></li>";
					}
					    

						?>

					</ul>
					</li>
                    </ul>
                    
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Manage"><i class="ti-settings"></i></a>
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Feedbacks"><i class="ti-comments"></i></a>
                <!-- item--><a href="logout.php" class="link" data-toggle="tooltip" title="Logout"><i class="ti-power-off"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
        <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Attendance</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Attendance</a></li>
                            <li class="breadcrumb-item active">Branch_Attendance</li>
                        </ol>
                    </div>
                   
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->


 <!-- Column -->
					
<div class="card">
			<div class="card-block">
			
				
				<!--<div class="col-lg-2">
				<label for="n1"></label>
				<br/>
				 <input type="submit" class="btn btn-info" name="n3" value="Show Data"/>
				</div>-->
				
				<iframe src="HOD_reports_frame.php" width="1020" height="1500" frameborder="0">
</iframe>  
            </div>        
				
</div>
  </tbody>
				   </table>
				   </td>
				   </tr>
				   </table>
				   
	
	
		



</div>
  </tbody>
				   </table>
					</div>  
                </div>   
            
</form>
</div>
</div>
</div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
			
            </div>
			
		            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
          <a href="About.php"><footer class="footer"> © 2018, GOVERNMENT OF GUJARAT ( Developed by Attendance Spy )</footer></a>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="../assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 JavaScript -->
    <script src="../assets/plugins/d3/d3.min.js"></script>
    <script src="../assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="js/dashboard1.js"></script>
</body>

</html>